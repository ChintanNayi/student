import React from "react";
import { StyleSheet } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
var { Platform } = React;

const WlcStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-between",
  },
  image: {
    alignItems: "center",
    justifyContent: "center",
    width: wp("50%"),
    height: Platform.OS === "ios" ? hp("9%") : hp("10%"),
    marginTop: Platform.OS === "ios" ? '26%' : '15%',
  },
  mainImg: {
    marginTop: Platform.OS === "ios" ? '18%' : '22%',
  },
  textbottom: {
    height: wp("10%"),
    color: "#696969",
  },
  bottom: {
    flex: 1,
    justifyContent: "flex-end",
  },
});

const loginStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  head: {
    fontWeight: "bold",
    textAlign: "center",
    paddingTop: 30,
    paddingBottom: 30,
    fontSize: hp("4%"),
    color: "#4d4d4d",
  },
  title: {
    height: hp("4%"),
    marginBottom: 20,
    textAlign: "center",
    color: "grey",
    marginTop: 20,
  },
  input: {
    height: hp("6%"),
    width: wp("80%"),
    borderColor: "gray",
    borderBottomWidth: 1,
    margin: 10,
  },
  loginText: {
    color: "white",
    fontWeight: "500",
  },
  loginBtn: {
    width: wp("80%"),
    borderRadius: 10,
    height: hp("5%"),
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
    marginBottom: 10,
    backgroundColor: "#00a64f",
  },
  image: {
    alignItems: "center",
    justifyContent: "center",
    width: wp("50%"),
    height: Platform.OS === "ios" ? hp("9%") : hp("10%"),
    marginTop: Platform.OS === "ios" ? '14%' : '15%',
    marginBottom: '10%',
  },
  forgot_button: {
    textAlign: "right",
    flex: 1,
    marginRight: 50,
    marginTop: 20,
    color: "#696969",
  },
  iconStyle: {
    marginTop: 30,
    marginLeft: 10, // default is 12
  },
  searchSection: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  bottom: {
    flex: 1,
    justifyContent: "flex-end",
  },
  errorMsg: {
    color: "#FF0000",
    fontSize: 10,
  },
});

const fpwdstyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    // marginTop: 10,
  },
  head: {
    fontWeight: "bold",
    textAlign: "center",
    paddingTop: 50,
    paddingBottom: 50,
    fontSize: hp("3%"),
    color: "#4d4d4d",
  },
  loginText: {
    color: "white",
    fontWeight: "500",
  },
  loginBtn: {
    width: wp("80%"),
    borderRadius: 10,
    height: hp("5%"),
    alignItems: "center",
    justifyContent: "center",
    marginTop: 50,
    marginBottom: 10,
    backgroundColor: "#00a64f",
  },
  image: {
    alignItems: "center",
    justifyContent: "center",
    width: wp("52%"),
    height: hp("10%"),
    marginTop: Platform.OS === "ios" ? '14%' : '16%',
    marginBottom: '10%',
  },
  errorMsg: {
    color: "#FF0000",
    fontSize: 10,
  },
});

const EntStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  head: {
    fontWeight: 'bold',
    textAlign: 'center',
    paddingTop: 40,
    fontSize: 25,
    color: '#4d4d4d'
  },
  subHead: {
    fontWeight: 'bold',
    textAlign: 'center',
    paddingTop: '3%',
    color: '#007419',
    paddingBottom: '8%'
  },
  loginText: {
    color: 'white',
    fontWeight: '500',
  },
  loginBtn:
  {
    width: wp('80%'),
    borderRadius: 10,
    height: hp("5%"),
    alignItems: "center",
    justifyContent: "center",
    marginTop: '15%',
    backgroundColor: "#00a64f",
  },
  image: {
    alignItems: 'center',
    justifyContent: 'center',
    width: wp('50%'),
    height: (Platform.OS === 'ios') ? hp('9%') : hp('10%'),
    marginTop: Platform.OS === "ios" ? '26%' : '16%',
    marginBottom: '10%',
  },
  iconStyle: {
    marginTop: 30,
    marginLeft: 10 // default is 12
  },
  inputGroup: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 40
  },
  resend: {
    textAlign: 'center',
    color: 'grey'
  },
  resend1: {
    textAlign: 'center',
    color: 'grey'
  },
  underlineStyleBase:
  {
    width: 30,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 3,
    color: 'black'
  },
  underlineStyleHighLighted:
  {
    borderColor: 'black',
    borderBottomColor: "#50ad50",
  },
  timerView: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 30,
    marginBottom: 20
  }
});

const GOTPstyles = StyleSheet.create({
  container: {
    flex: 1,
    // textAlign:'center',
    alignItems: "center",
  },
  loginText: {
    color: "white",
    fontWeight: "500",
  },
  head: {
    fontWeight: "bold",
    textAlign: "center",
    paddingTop: '10%',
    fontSize: 30,
    color: "#4d4d4d",
  },
  loginBtn: {
    width: wp("80%"),
    borderRadius: 10,
    height: hp("5%"),
    alignItems: "center",
    justifyContent: "center",
    marginTop: '12%',
    marginBottom: 10,
    backgroundColor: "#00a64f",
  },
  image: {
    alignItems: "center",
    justifyContent: "center",
    width: wp("50%"),
    height: Platform.OS === "ios" ? hp("9%") : hp("10%"),
    marginTop: '15%',
    marginBottom: '10%',
  },
  iconStyle: {
    marginTop: 30,
    marginLeft: 10, // default is 12
  },
  searchSection: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  radioGroup: {
    flexDirection: "row",
    width: wp("80%"),
  },
  radioText: {
    marginTop: 7,
    textAlign: "left",
  },
  buttonContainer: {
    flexDirection: "row",
    marginBottom: 20,
  },
  circle: {
    height: 20,
    width: 20,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: "#feba24",
    alignItems: "center",
    justifyContent: "center",
  },
  checkedCircle: {
    width: 10,
    height: 10,
    borderRadius: 10,
    backgroundColor: "#feba24",
  },
});

const EOTPstyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  head: {
    fontWeight: "bold",
    textAlign: "center",
    paddingTop: 40,
    fontSize: 25,
    color: "#4d4d4d",
  },
  loginText: {
    color: "white",
    fontWeight: "500",
  },
  loginBtn: {
    width: wp("80%"),
    borderRadius: 10,
    height: hp("5%"),
    alignItems: "center",
    justifyContent: "center",
    marginTop: '15%',
    backgroundColor: "#00a64f",
  },
  image: {
    alignItems: "center",
    justifyContent: "center",
    width: wp("50%"),
    height: Platform.OS === "ios" ? hp("9%") : hp("10%"),
    marginTop: Platform.OS === "ios" ? '26%' : '16%',
    marginBottom: '5%'
  },
  iconStyle: {
    marginTop: 30,
    marginLeft: 10, // default is 12
  },
  inputGroup: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 40,
  },
  resend: {
    textAlign: "center",
    color: "#00a64f",
  },
  resend1: {
    textAlign: "center",
    color: "grey",
  },
  underlineStyleBase: {
    width: 30,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 3,
    color: "black",
  },
  underlineStyleHighLighted: {
    borderColor: "black",
    borderBottomColor: "#50ad50",
  },
  timerView: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 30,
    marginBottom: 20,
  },
});

const dashboardStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  head: {
    flexDirection: "row",
    marginTop: Platform.OS === "ios" ? 40 : 10,
    width: wp("80%"),
    justifyContent: "space-between",
    marginLeft: Platform.OS === "ios" ? 0 : 20,
  },
  title: {
    color: "grey",
    fontWeight: "bold",
    fontSize: 20,
  },
  userView: {
    flexDirection: "row",
    marginTop: 40,
    width: wp("90%"),
    justifyContent: "space-between",
    zIndex: 1,
  },
  user: {
    color: "black",
    fontWeight: "bold",
    fontSize: 20,
  },
  image: {
    width: wp("8%"),
    height: Platform.OS === "ios" ? hp("3%") : hp("4%"),
    alignSelf: "flex-end",
    marginLeft: Platform.OS === "ios" ? 22 : 0,
  },
  imgView: {
    backgroundColor: "#FFFFFF",
    width: wp("10%"),
    height: hp("4.5%"),
    borderRadius: 4,
    shadowColor: "#000000",
    shadowOpacity: Platform.OS === "ios" ? 0.2 : 0,
    shadowRadius: Platform.OS === "ios" ? 15 : 0,
    zIndex: 0,
  },
  mainView: {
    top: '5%',
    borderRadius: 4,
  },
  itemView: {
    height: hp("20%"),
    width: wp("35%"),
    borderRadius: 10,
    margin: 20,
    alignItems: "center",
    shadowOpacity: 0.3, // <- and this or yours opacity
    shadowRadius: 15,
    borderRadius: 10,
    backgroundColor: "#ECF0F3",
  },
  profileView: {
    width: wp("20%"),
    backgroundColor: "white",
    alignContent: "center",
    borderRadius: 5,
    right: 40,
    padding: 5,
    marginTop: Platform.OS === "ios" ? '18%' : '10%',
    shadowColor: "#000000",
    shadowOpacity: Platform.OS === "ios" ? 0.2 : 0,
    shadowRadius: Platform.OS === "ios" ? 15 : 0,
    zIndex: 2,
  },
});
const feedBackstyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  mainView: {
    width: wp("95%"),
    backgroundColor: "#FFFFFF",
    marginTop: 10,
    borderRadius: 5,
    paddingBottom: 10,
  },
  subView: {
    flexDirection: "row",
    // justifyContent: 'space-between',
    alignItems: "flex-start",
    alignContent: "flex-start",
  },
  headTxt: {
    color: "black",
    fontWeight: "bold",
    paddingTop: 10,
    paddingLeft: 20,
  },
  subTxt: {
    left: 85,
    paddingTop: 10,
    color: "black",
  },
  txtName: {
    left: 35,
    paddingTop: 10,
    color: "black",
    width: "auto",
  },
  txtTime: {
    left: 70,
    paddingTop: 10,
    color: "black",
  },
  txtClassName: {
    left: 65,
    paddingTop: 10,
    color: "black",
  },
});

const timesheetStyles = StyleSheet.create({
  container: {
    // flex: 1,
    alignItems: "center",
  },
  head: {
    flexDirection: "row",
    marginTop: 50,
    width: wp("80%"),
    justifyContent: "space-between",
    marginLeft: 30,
  },
  title: {
    color: "grey",
    fontWeight: "bold",
    fontSize: 20,
  },
  image: {
    width: wp("7%"),
    height: hp("3%"),
    alignSelf: "flex-end",
    marginLeft: 20,
  },
  userView: {
    flexDirection: "row",
    marginTop: 40,
    width: wp("90%"),
    justifyContent: "space-between",
    paddingBottom: 10,
  },
  user: {
    color: "black",
    fontWeight: "bold",
    fontSize: 20,
    paddingTop: 5,
    color: "#00a64f",
  },
  userImg: {
    width: wp("7%"),
    height: hp("3%"),
    alignSelf: "center",
    marginTop: 3,
  },
  imgView: {
    backgroundColor: "#FFFFFF",
    width: wp("10%"),
    height: hp("4%"),
    borderRadius: 4,
  },
  mainView: {
    width: wp("95%"),
    backgroundColor: "#FFFFFF",
    marginTop: 20,
    borderRadius: 5,
    paddingBottom: 10,
  },
});

const payNowStyles = StyleSheet.create({
  container: {
    alignItems: "center",
  },
  mainView: {
    width: wp("95%"),
    backgroundColor: "#FFFFFF",
    marginTop: 45,
    borderRadius: 5,
    padding: 10,
  },
  input: {
    height: hp("7%"),
    width: wp("40%"),
    borderColor: "grey",
    borderBottomWidth: 1,
  },
  fullinput: {
    height: hp("7%"),
    width: wp("85%"),
    borderColor: "gray",
    borderBottomWidth: 1,
  },
  loginText: {
    color: "white",
    fontWeight: "500",
  },
  loginBtn: {
    width: wp("90%"),
    borderRadius: 10,
    height: hp("5%"),
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: "#00a64f",
    // marginLeft: 10
  },
});

const paymentsStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  header: {
    marginTop: 45,
    width: wp("95%"),
    backgroundColor: "#FFFFFF",
    borderRadius: 4,
    justifyContent: "space-between",
    flexDirection: "row",
    padding: 10,
  },
  mainView: {
    width: wp("95%"),
    height: hp("15%"),
    backgroundColor: "#FFFFFF",
    marginTop: 10,
    borderRadius: 5,
  },
  emptyMessageStyle: {
    textAlign: 'center',
    marginTop: '50%',
  },
  subView: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    alignContent: "flex-start",
  },
  txtTypeaction: {
    color: "black",
    fontWeight: "bold",
    paddingTop: 10,
    paddingLeft: 20,
  },
  txtSub: {
    paddingRight: '26%',
    paddingTop: 10,
    color: "black",
  },
  txtTime: {
    paddingRight: '16%',
    paddingTop: 10,
    color: "black",
  },
  txtStatus: {
    paddingRight: '48%',
    paddingTop: 10,
    color: "black",
  },
  txtDesc: {
    paddingRight: Platform.OS === 'ios' ? 13 : 20,
    paddingTop: 10,
    color: "black"
  },
  headTitle: {
    color: "black",
    fontWeight: "bold",
  },
  compliteView: {
    backgroundColor: "#00a64f",
    alignItems: "center",
    marginRight: 20,
    marginTop: 13,
    width: wp("25%"),
    // height: hp('3%'),
    borderRadius: 15,
  },
  paddingView: {
    backgroundColor: "#fba00c",
    alignItems: "center",
    marginRight: 20,
    marginTop: 13,
    width: wp("25%"),
    // height: hp('3%'),
    borderRadius: 15,
  },
  input: {
    height: hp("5%"),
    width: wp("40%"),
    borderColor: "gray",
    borderBottomWidth: 1,
  },
  fullinput: {
    height: hp("5%"),
    width: wp("90%"),
    borderColor: "gray",
    borderBottomWidth: 1,
  },
  loginText: {
    color: "white",
    fontWeight: "500",
  },
  loginBtn: {
    width: wp("90%"),
    height: hp("5%"),
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: "#00a64f",
    marginLeft: 10,
  },
});

const makePaymentStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  header: {
    marginTop: 45,
    width: wp("95%"),
    backgroundColor: "#FFFFFF",
    borderRadius: 4,
    justifyContent: "space-between",
    flexDirection: "row",
    padding: 10,
  },
  mainView: {
    width: wp("95%"),
    height: hp("15%"),
    backgroundColor: "#FFFFFF",
    marginTop: 10,
    borderRadius: 5,
  },
  showView: {
    width: wp("95%"),
    height: hp("30%"),
    backgroundColor: "#FFFFFF",
    marginTop: '20%',
    borderRadius: 5,
  },
  showView1: {
    width: wp("95%"),
    height: hp("30%"),
    backgroundColor: "#FFFFFF",
    borderRadius: 5,
  },
  emptyMessageStyle: {
    textAlign: 'center',
    marginTop: '50%',
  },
  subView: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    alignContent: "flex-start",
  },
  txtTypeaction: {
    color: "black",
    fontWeight: "bold",
    paddingTop: 10,
    paddingLeft: 20,
  },
  txtSub: {
    paddingRight: '26%',
    paddingTop: 10,
    color: "black",
  },
  txtTime: {
    paddingRight: '16%',
    paddingTop: 10,
    color: "black",
  },
  txtStatus: {
    paddingRight: '48%',
    paddingTop: 10,
    color: "black",
  },
  txtDesc: {
    paddingRight: Platform.OS === 'ios' ? 13 : 20,
    paddingTop: 10,
    color: "black"
  },
  headTitle: {
    color: "black",
    fontWeight: "bold",
  },
  compliteView: {
    backgroundColor: "#00a64f",
    alignItems: "center",
    marginRight: 20,
    marginTop: 13,
    width: wp("25%"),
    // height: hp('3%'),
    borderRadius: 15,
  },
  paddingView: {
    backgroundColor: "#fba00c",
    alignItems: "center",
    marginRight: 20,
    marginTop: 13,
    width: wp("25%"),
    height: hp('2%'),
    borderRadius: 15,
  },
  input: {
    height: hp("5%"),
    width: wp("40%"),
    borderColor: "gray",
    borderBottomWidth: 1,
  },
  fullinput: {
    height: hp("5%"),
    width: wp("90%"),
    borderColor: "gray",
    borderBottomWidth: 1,
  },
  loginText: {
    color: "white",
    fontWeight: "500",
  },
  loginBtn: {
    width: wp("90%"),
    borderRadius: 10,
    height: hp("5%"),
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: "#00a64f",
    marginLeft: 10,
  },
});

const profileStyles = StyleSheet.create({
  container: {
    alignItems: "center",
    flex: 1,
  },
  mainView: {
    width: wp("95%"),
    backgroundColor: "#FFFFFF",
    marginTop: 15,
    borderRadius: 5,
    padding: 10,
  },
  input: {
    height: hp("7%"),
    width: wp("40%"),
    borderColor: "gray",
    borderBottomWidth: 1,
  },
  fullinput: {
    height: hp("7%"),
    width: wp("85%"),
    borderColor: "gray",
    borderBottomWidth: 1,
  },
  loginText: {
    color: "white",
    fontWeight: "500",
  },
  loginBtn: {
    width: wp("90%"),
    height: hp("5%"),
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: "#00a64f",
  },
});

const historyStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  mainView: {
    width: wp("95%"),
    backgroundColor: "#FFFFFF",
    marginTop: 10,
    borderRadius: 5,
    marginBottom:10
  },
  subView: {
    flexDirection: "column",
    // justifyContent: "space-between",
    // alignItems: "flex-start",
    // alignContent: "flex-start",
  },
  txtTypeaction: {
    color: "black",
    fontWeight: "bold",
    paddingTop: 5,
    paddingLeft: 20,
  },
  txtSub: {
    // paddingRight: '26%',
    paddingLeft: 20,
    color: "black",
  },
  txtTime: {
    // paddingRight: '16%',
    paddingLeft: 20,
    color: "black",
  },
  txtDesc: {
    paddingLeft: 20,
    paddingBottom:10,
    // paddingRight: Platform.OS === 'ios' ? 13 : 20,
    color: "black",
  },
});

export {
  WlcStyles,
  loginStyles,
  fpwdstyles,
  GOTPstyles,
  EOTPstyles,
  dashboardStyles,
  feedBackstyles,
  timesheetStyles,
  makePaymentStyles,
  payNowStyles,
  paymentsStyles,
  profileStyles,
  historyStyles,
  EntStyles,
};
