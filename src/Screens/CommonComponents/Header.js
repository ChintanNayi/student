import React, { useState, Component } from 'react'
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity, Button, ScrollView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import logo from '../../Assets/dsc-logo.png'
import { Avatar } from 'react-native-elements';
import FIcon from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';

var { Platform } = React;
const Header = (props) => {
    const navigation = useNavigation();
    const [isShowing, setIsShowing] = useState(false);

    return (
        <View style={styles.container}>
            <View style={styles.head}>
                <Text style={styles.title}>
                    Blue Mountain Driving School
                </Text>
                <Image style={styles.image} source={logo} />
            </View>
            <View style={styles.userView}>
                <View style={styles.imgView}>
                    <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
                        <Icon name="home" style={{ paddingTop: 5, paddingLeft: 7 }} color='grey' size={25} />
                    </TouchableOpacity>
                </View>

                <Text style={styles.user}>
                    {props.title}
                </Text>
                <View style={styles.imgView}>
                    <Avatar
                        containerStyle={{ alignSelf: 'center', backgroundColor: 'grey', marginTop: 7 }}
                        rounded
                        icon={{ name: 'user-alt', type: 'font-awesome-5' }}
                        onAccessoryPress={() => Alert.alert("change avatar")}
                        overlayContainerStyle={{ backgroundColor: 'grey' }}
                        size={25}
                        onPress={() => setIsShowing(!isShowing)}
                    />
                    {isShowing === true ?
                        <View style={styles.profileView}>
                            <TouchableOpacity onPress={() => { setIsShowing(!isShowing), navigation.navigate('Profile') }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Avatar
                                        containerStyle={{ alignSelf: 'center', backgroundColor: 'grey', }}
                                        rounded
                                        icon={{ name: 'user-alt', type: 'font-awesome-5' }}
                                        onAccessoryPress={() => Alert.alert("change avatar")}
                                        overlayContainerStyle={{ backgroundColor: 'grey' }}
                                        size={20}
                                        onPress={() => console.log("Works!")}
                                    />
                                    <Text style={{ color: 'grey', paddingLeft: 5, fontSize: 15 }}>Profile</Text>
                                </View>
                            </TouchableOpacity>
                            <View style={{ borderWidth: 1, borderColor: 'grey', marginTop: 4 }} />
                            <TouchableOpacity onPress={() => {
                                setIsShowing(!isShowing),
                                    AsyncStorage.clear();
                                navigation.navigate('LoginFlow')
                            }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <FIcon name="log-out" style={{ paddingTop: 3, }} color='grey' size={20} />
                                    <Text style={{ color: 'grey', paddingLeft: 5, fontSize: 15, paddingTop: 3 }}>Log Out</Text>
                                </View>
                            </TouchableOpacity>
                        </View> : null}
                </View>
            </View>
        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        alignItems: 'center',
        zIndex: 1,
    },
    head: {
        flexDirection: 'row',
        marginTop: (Platform.OS === 'ios') ? 40 : 10,
        width: wp('80%'),
        justifyContent: 'space-between',
        marginLeft: 30,
    },
    title: {
        color: 'grey',
        fontWeight: 'bold',
        fontSize: 20
    },
    image: {
        width: wp('8%'),
        height: hp('4%'),
        alignSelf: 'flex-end',
        marginLeft: Platform.OS === "ios" ? 10 : 20,
    },
    userView: {
        flexDirection: 'row',
        marginTop: 25,
        width: wp('90%'),
        justifyContent: 'space-between',
        paddingBottom: 10
    },
    user: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 20,
        paddingTop: 5,
        color: '#00a64f'
    },
    userImg: {
        width: wp('7%'),
        // height: hp('4%'),
        alignSelf: 'center',
        // marginTop: 3
    },
    imgView: {
        backgroundColor: '#FFFFFF',
        width: wp('10%'),
        height: hp('4.5%'),
        borderRadius: 4,
        shadowColor: '#000000',
        elevation: 10,
        borderRadius: 4,
        shadowOpacity: Platform.OS === "ios" ? 0.2 : 0,
        shadowRadius: Platform.OS === "ios" ? 15 : 0,
    },
    profileView: {
        width: wp('25%'),
        backgroundColor: 'white',
        alignContent: 'center',
        borderRadius: 5,
        top: '100%',
        padding: 5,
        position: 'absolute',
        shadowColor: '#000000',
        elevation: 10,
        shadowOpacity: Platform.OS === "ios" ? 0.2 : 0,
        shadowRadius: Platform.OS === "ios" ? 15 : 0,
        right: '0%',

    }
})

export default Header;