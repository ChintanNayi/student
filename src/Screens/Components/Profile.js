import React, { Component } from 'react'
import { Text, View, TextInput, ScrollView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Avatar } from 'react-native-elements';
import Header from '../CommonComponents/Header'
import Ripple from 'react-native-material-ripple';
import ImagePicker from 'react-native-image-crop-picker';
import DatePicker from 'react-native-datepicker';
import { profileStyles } from '../CommonComponents/css';
import { connect } from "react-redux";
import { ProfileData } from '../../Redux/Action/profileAction';
import Toast from 'react-native-toast-message';
var { Platform } = React;
import { Spinner } from "native-base";
import * as Actions from '../../Redux/Action/profileAction';

export class Profile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            fname: "",
            mname: "",
            lname: "",
            dob: "",
            mobile: "",
            email: "",
            addres: "",
        }
        this.takePhotoFromCamera = this.takePhotoFromCamera.bind(this);
    }

    componentDidMount = async () => {
        await this.props.stateDispatch(this.props.token);
       console.log("data",this.props.profileState)
    }

    takePhotoFromCamera = async () => {
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true,
        }).then(image => {
            console.log(image);
        });
    }

    showToast = () =>
        Toast.show({
            visibilityTime: 3000,
            position: 'top',
            text1: 'You have signed in successfully.',
            topOffset: Platform.OS === "ios" ? 40 : 5,
        });

    handleSave = async () => {
        this.setState({ loaded: true });
        let obj = {
            first_name: this.state.fname,
            middle_name: this.state.mname,
            last_name: this.state.lname,
            dob: this.state.dob,
            mobile: this.state.mobile,
            email: this.state.email,
            address: this.state.addres
        }
        await this.props.stateDispatch(obj);
        this.showToast();
        setTimeout(() => {
            this.setState({ loaded: false });
        }, 1000);

    }

    render() {
        return (
            <View style={profileStyles.container}>
                <Toast />
                <Header title={'Edit Profile'} />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={profileStyles.mainView}>
                        <Avatar
                            containerStyle={{ alignSelf: 'center', backgroundColor: 'grey' }}
                            rounded
                            source={{
                                uri:
                                    'https://www.mcicon.com/wp-content/uploads/2021/01/People_User_1-copy-4.jpg',
                            }}
                            onAccessoryPress={() => Alert.alert("change avatar")}
                            overlayContainerStyle={{ backgroundColor: 'grey' }}
                            size={hp('15%')}
                        >
                            <Avatar.Accessory size={28} onPress={this.takePhotoFromCamera} />
                        </Avatar>
                        <Text style={{ alignSelf: 'center', paddingTop: 10, color: 'grey' }}>Change Profile Picture</Text>
                        <View>
                            <Text style={{ color: 'black' }}>Student Id</Text>
                            <Text style={{ color: 'grey' }}>#1234</Text>
                        </View>
                        <View style={{ paddingTop: 10 }}>
                            <Text style={{ paddingTop: 5, color: 'black' }}>First Name</Text>
                            <TextInput
                                style={profileStyles.fullinput}
                                onChangeText={(text) => this.setState({ fname: text })}
                                placeholder='Smith'
                                underlineColorAndroid="transparent"
                                value={this.state.fname}
                            />
                            <Text style={{ paddingTop: 10, color: 'black' }}>Middle Name</Text>
                            <TextInput
                                style={profileStyles.fullinput}
                                onChangeText={(text) => this.setState({ mname: text })}
                                placeholder='Hery'
                                underlineColorAndroid="transparent"
                                value={this.state.mname}
                            />
                            <Text style={{ paddingTop: 10, color: 'black' }}>Last Name</Text>
                            <TextInput
                                style={profileStyles.fullinput}
                                onChangeText={(text) => this.setState({ lname: text })}
                                placeholder='Joe'
                                underlineColorAndroid="transparent"
                                value={this.state.lname}
                            />
                            <Text style={{ paddingTop: 10, paddingBottom: 10, color: 'black' }}>Date of Birth</Text>
                            <DatePicker
                                style={{ width: wp('85%'), paddingBottom: 10 }}
                                date={this.state.dob}
                                mode="date"
                                placeholder="Enter Date Of Birth"
                                format="DD-MM-YYYY"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                showIcon={false}
                                customStyles={{
                                    dateInput: {
                                        borderWidth: 0,
                                        borderBottomWidth: 1,
                                        alignItems: 'baseline',
                                    },
                                    placeholderText: {
                                        color: 'grey'
                                    }
                                    // ... You can check the source to find the other keys.
                                }}
                                onDateChange={(date) => { this.setState({ dob: date }) }}
                            />
                            <Text style={{ paddingTop: 10, color: 'black' }}>Mobile No</Text>
                            <TextInput
                                style={profileStyles.fullinput}
                                onChangeText={(text) => this.setState({ mobile: text })}
                                placeholder='+91 1234567809'
                                underlineColorAndroid="transparent"
                                value={this.state.mobile}
                                keyboardType="number-pad"
                            />
                            <Text style={{ paddingTop: 10, color: 'black' }}>Email-Id</Text>
                            <TextInput
                                style={profileStyles.fullinput}
                                onChangeText={(text) => this.setState({ email: text })}
                                placeholder='user@gmail.com'
                                underlineColorAndroid="transparent"
                                value={this.state.email}
                            />
                            <Text style={{ paddingTop: 10, color: 'black' }}>Address</Text>
                            <TextInput
                                style={profileStyles.fullinput}
                                onChangeText={(text) => this.setState({ addres: text })}
                                placeholder='lorem sum'
                                underlineColorAndroid="transparent"
                                value={this.state.addres}
                                multiline
                                numberOfLines={7}
                            />
                        </View>
                        {this.state.loaded === true ? (
                            <Spinner style={{ marginTop: 30 }} size={"lg"} color="#f0bf4c" />
                        ) : (
                            <Ripple onPress={this.handleSave} rippleColor="#99FF99" style={profileStyles.loginBtn}>
                                <Text style={profileStyles.loginText}>Save</Text>
                            </Ripple>
                        )}

                    </View>
                </ScrollView>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    const { token } = state.auth;
    const { profile } = state.profileReducer;
    console.log("token",token)
    return { 
        profileState: profile,
        token: token,
    };
}

const mapDispatchToProps = (dispatch) => ({
    stateDispatch: (token) => dispatch(Actions.UserDetails(token))
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);