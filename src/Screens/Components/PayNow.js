import React, { Component } from 'react'
import { Text, View, TextInput, ScrollView } from 'react-native'
import Header from '../CommonComponents/Header';
import Icon from 'react-native-vector-icons/FontAwesome5';
import EIcon from 'react-native-vector-icons/EvilIcons';
import Ripple from 'react-native-material-ripple';
import { payNowStyles } from '../CommonComponents/css';
import { connect } from "react-redux";
import { AddPayment } from '../../Redux/Action/makpaymentAction';
import Toast from 'react-native-toast-message';
var { Platform } = React;
import { Spinner } from "native-base";

export class PayNow extends Component {
    constructor(props) {
        super(props)

        this.state = {
            cname: "",
            cnumber: "",
            exdate: "",
            cvv: "",
            total: ""
        }
    }

    showToast = () =>
        Toast.show({
            visibilityTime: 3000,
            position: 'top',
            text1: 'You have signed in successfully.',
            topOffset: Platform.OS === "ios" ? 40 : 5,
        });

    handlePay = async () => {
        this.setState({ loaded: true });
        let obj = {
            name: this.state.cname,
            card_number: this.state.cnumber,
            expaire_date: this.state.exdate,
            cvv: this.state.cvv,
            amount: this.state.total
        }
        await this.props.stateDispatch(obj);
        this.showToast();
        setTimeout(() => {
            this.setState({ loaded: false });
        }, 1000);

    }

    render() {
        return (
            <View style={payNowStyles.container}>
                <Toast />
                <Header title={'Make Payments'} />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={payNowStyles.mainView}>
                        <View style={{ flexDirection: 'row' }}>
                            <EIcon name="lock" color='grey' size={25} />
                            <Text style={{ color: 'black' }}>SECURE SSL ENCRIPTED PAYMENT</Text>
                        </View>
                        <View style={{ flexDirection: 'row', padding: 5 }}>
                            <Icon name="cc-mastercard" color='green' size={35} />
                            <Icon name="credit-card" color='green' style={{ marginLeft: 10 }} size={35} />
                        </View>
                        <Text style={{ color: 'black', padding: 5, fontSize: 15 }}>Payment Information (required)</Text>
                        <Text style={{ color: 'black', padding: 5 }}>CREDIT CARD/DEBIT CARD</Text>
                        <View style={{ left: '1%' }}>
                            <Text style={{ color: 'black', top: 10 }}>Card holder name</Text>
                            <TextInput
                                style={payNowStyles.fullinput}
                                onChangeText={(text) => this.setState({ cname: text })}
                                placeholder='Enter card holder name'
                                underlineColorAndroid="transparent"
                                value={this.state.cname}
                            />
                        </View>
                        <View style={{ left: '1%' }}>
                            <Text style={{ color: 'black', top: 10 }}>Card number</Text>
                            <TextInput
                                style={payNowStyles.fullinput}
                                onChangeText={(text) => this.setState({ cnumber: text })}
                                placeholder='Enter Card number'
                                underlineColorAndroid="transparent"
                                value={this.state.cnumber}
                                keyboardType="number-pad"
                            />
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', left: 4 }}>

                            <View>
                                <Text style={{ color: 'black', top: 10 }}>Add month/year</Text>
                                <TextInput
                                    style={payNowStyles.input}
                                    onChangeText={(text) => this.setState({ exdate: text })}
                                    placeholder='MM/YY'
                                    underlineColorAndroid="transparent"
                                    value={this.state.exdate}
                                    keyboardType="number-pad"
                                    maxLength={5}
                                />
                            </View>
                            <View style={{ right: 20 }}>
                                <Text style={{ color: 'black', top: 10 }}>CVV number</Text>
                                <TextInput
                                    style={payNowStyles.input}
                                    onChangeText={(text) => this.setState({ cvv: text })}
                                    placeholder='Add CVV number'
                                    underlineColorAndroid="transparent"
                                    value={this.state.cvv}
                                    secureTextEntry
                                    maxLength={3}
                                    keyboardType='phone-pad'
                                />
                            </View>

                        </View>
                        <View style={{ left: 4 }}>
                            <Text style={{ color: 'black', top: 10 }}>Total Amount</Text>
                            <TextInput
                                style={payNowStyles.fullinput}
                                onChangeText={(text) => this.setState({ total: text })}
                                placeholder='$260'
                                underlineColorAndroid="transparent"
                                value={this.state.total}
                                keyboardType="number-pad"
                            />
                        </View>
                        {this.state.loaded === true ? (
                            <Spinner style={{ marginTop: 30 }} size={"lg"} color="#f0bf4c" />
                        ) : (
                            <Ripple onPress={this.handlePay} rippleColor="#99FF99" style={payNowStyles.loginBtn}>
                                <Text style={payNowStyles.loginText}>Pay</Text>
                            </Ripple>
                        )}

                    </View>
                </ScrollView>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    const { makePaymnent } = state.makePaymentReducer;
    return { paymentState: makePaymnent };
}

const mapDispatchToProps = (dispatch) => ({
    stateDispatch: (data) => dispatch(AddPayment(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(PayNow);
