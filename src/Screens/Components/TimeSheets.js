import React, { Component } from 'react'
import { Text, View, TouchableOpacity, ScrollView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FIcon from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/FontAwesome5';
import CalendarStrip from 'react-native-calendar-strip';
import Header from '../CommonComponents/Header';
import { timesheetStyles } from '../CommonComponents/css';
import { connect } from "react-redux";
import { TimeSheetList } from '../../Redux/Action/timeSheetAction';

export class TimeSheets extends Component {
    constructor(props) {
        super(props)

        this.state = {
            show: false,
            selectedDate: '',
            date: new Date()
        }
    }

    componentDidMount = async () => {
        await this.props.stateDispatch();
        console.log("time sheet data", this.props.timesheetState)
    }

    render() {
        return (
            <View style={timesheetStyles.container}>
                <Header title={'Timesheet'} />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View>
                        <CalendarStrip
                            scrollable
                            calendarAnimation={{ type: 'sequence', duration: 30 }}
                            selectedDate={this.state.date}
                            onDateSelected={date => this.setState({ selectedDate: date })}
                            daySelectionAnimation={{ type: 'border', duration: 200, borderWidth: 1, borderHighlightColor: '#00a64f' }}
                            style={{ height: hp('10%'), backgroundColor: 'transparent' }}
                            calendarHeaderStyle={{ color: 'black' }}
                            calendarColor={'#7743CE'}
                            dateNumberStyle={{ color: 'black' }}
                            dateNameStyle={{ color: 'black' }}
                            highlightDateNumberStyle={{ color: '#00a64f' }}
                            highlightDateNameStyle={{ color: '#00a64f' }}
                            disabledDateNameStyle={{ color: '#00a64f' }}
                            disabledDateNumberStyle={{ color: '#00a64f' }}
                            iconContainer={{ flex: 0.1 }}
                        />


                        <View style={timesheetStyles.mainView}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

                                <View style={{ flexDirection: 'row' }}>
                                    <FIcon name="file-text" style={{ paddingTop: 3, paddingLeft: 5 }} color='#feba24' size={45} />
                                    <View style={{ flexDirection: 'column', paddingTop: 5 }}>
                                        <View style={{ flexDirection: 'row', paddingLeft: 5 }}>
                                            <Text style={{ color: '#00a64f' }}>Course 1</Text>
                                            <Text style={{ paddingLeft: 5 }}>(Theory)</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', paddingLeft: 5, paddingTop: 5 }}>
                                            <Text style={{ color: 'grey' }}>Start Time :10am</Text>
                                            <Text style={{ paddingLeft: 5, color: 'grey' }}>End Time :12pm</Text>
                                        </View>
                                    </View>
                                </View>

                                {
                                    this.state.show === true ?
                                        <TouchableOpacity onPress={() => this.setState({ show: !this.state.show })}>
                                            <Icon name="chevron-circle-up" color='grey' style={{ paddingRight: 15, paddingTop: 20 }} size={30} />
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity onPress={() => this.setState({ show: !this.state.show })}>
                                            <Icon name="chevron-circle-down" color='grey' style={{ paddingRight: 15, paddingTop: 20 }} size={30} />
                                        </TouchableOpacity>
                                }
                            </View>
                            {
                                this.state.show === true ?
                                    <View>
                                        <View style={{ width: wp('100%'), borderColor: 'grey', borderWidth: 1, marginTop: 10 }} />
                                        <View style={{ flexDirection: 'column', paddingTop: 5, alignItems: 'center' }}>
                                            <Text style={{ color: 'grey' }}>Instructore Add Start Time / End Time</Text>
                                            <View style={{ flexDirection: 'row', paddingLeft: 5, paddingTop: 5 }}>
                                                <Text style={{ color: 'grey', marginRight: 20 }}>Start Time : 10am</Text>
                                                <Text style={{ paddingLeft: 5, color: 'grey' }}>End Time : 12pm</Text>
                                            </View>
                                        </View>
                                    </View> : null}
                        </View>

                    </View>
                </ScrollView>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    const { timesheet } = state.timesheetReducer;
    return { timesheetState: timesheet };
}

const mapDispatchToProps = (dispatch) => ({
    stateDispatch: () => dispatch(TimeSheetList())
});

export default connect(mapStateToProps, mapDispatchToProps)(TimeSheets);;