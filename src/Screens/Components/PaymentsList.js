import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity, Button, ScrollView, FlatList } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Header from '../CommonComponents/Header'
import { Avatar, CheckBox } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Ripple from 'react-native-material-ripple';
import { paymentsStyles } from '../CommonComponents/css';
import { connect } from "react-redux";
import * as Actions from '../../Redux/Action/paymentAction';
import Moment from 'react-moment';
import { LIMIT, PAGE } from '../../Redux/const';

export class PaymentsList extends Component {

    constructor(props) {
        super(props)
        super(props)

        this.state = {
            data: [],
            // checked: false,
            // options: [
            //     {
            //         key: 'pay',
            //         text: 'Email Id',
            //     },
            //     {
            //         key: 'performance',
            //         text: 'Phone Number',
            //     },
            // ],
            // selectedOption: 'pay',
            // show: false
        }
        // this.onSelect = this.onSelect.bind(this);
    }

    // onSelect = (item) => {
    //     this.setState({ selectedOption: item })
    //     console.log("test", this.state.selectedOption, item)
    //     if (this.state.selectedOption === item.key) {
    //         this.setState({ selectedOption: null })
    //     } else {
    //         this.setState({ selectedOption: item.key })
    //     }
    // };

    componentDidMount = async () => {
        await this.props.stateDispatch(PAGE, LIMIT, this.props.token);
        this.setState({ data: this.props.paymentData.data });
        console.log("Payment List Data ==>>", this.state.data)

    //     if (this.state.data != []) {
    //         this.setState({ data: this.props.paymentData.data });
    //     } else {
    //         <View style={paymentsStyles.mainView}>
    //             <Text style={{ fontWeight: 'bold', textAlign: 'center' }}> Sorry, No Data Present... Try Again.</Text>
    //         </View>
    //     }
    }

    render() {
        return (
            <View style={paymentsStyles.container}>
                <Header title={'Payments'} />

                <FlatList
                    data={this.state.data}
                    renderItem={({ item }) =>
                        <View key={item.id} style={paymentsStyles.mainView}>
                            <View style={paymentsStyles.subView}>
                                <Text style={paymentsStyles.txtTypeaction}>Amount:</Text>
                                <Text style={paymentsStyles.txtTime} >{item.amount}</Text>
                            </View>
                            <View style={paymentsStyles.subView}>
                                <Text style={paymentsStyles.txtTypeaction}>Transaction ID:</Text>
                                <Text style={paymentsStyles.txtDesc}>{item.payment_id}</Text>
                            </View>
                            <View style={paymentsStyles.subView}>
                                <Text style={paymentsStyles.txtTypeaction}>Timestamp:</Text>
                                <Moment style={paymentsStyles.txtTime} format='MMM D, YYYY | h:mm:ss A' element={Text}>{item.last_updated}</Moment>
                            </View>
                            <View style={paymentsStyles.subView}>
                                <Text style={paymentsStyles.txtTypeaction}>Status</Text>
                                {item.status ?
                                    <Text style={paymentsStyles.txtStatus}>Successful</Text>
                                    :
                                    <Text style={paymentsStyles.txtStatus}>Failed</Text>
                                }

                            </View>
                        </View>
                    }
                />

                {/* <View style={paymentsStyles.header}>
                    <View style={{ flexDirection: 'column', alignItems: 'center', }}>
                        <Text style={paymentsStyles.headTitle}>Total Course Fee</Text>
                        <Text>$1000</Text>
                    </View>
                    <View style={{ flexDirection: 'column' }}>
                        <Text style={paymentsStyles.headTitle}>Fee Paid</Text>
                        <Text style={{ paddingLeft: 10, color: '#00a64f' }}>$300</Text>
                    </View>
                    <View style={{ flexDirection: 'column' }}>
                        <Text style={paymentsStyles.headTitle}>Remaining Fee</Text>
                        <Text style={{ paddingLeft: 30, color: '#fba00c' }}>$700</Text>
                    </View>
                </View>
                <ScrollView showsVerticalScrollIndicator={false}>

                    <View style={paymentsStyles.mainView}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start', alignContent: 'flex-start' }}>
                            <View>
                                <Text style={{ color: 'black', paddingTop: 10, paddingLeft: 10 }}>Program Name</Text>
                                <Text style={{ color: 'grey', paddingTop: 5, paddingLeft: 10 }}>Lorem Ipsum is dummy Text</Text>
                            </View>
                            {
                                this.state.show === true ?
                                    <TouchableOpacity onPress={() => this.setState({ show: !this.state.show })}>
                                        <Icon name="chevron-circle-up" color='grey' style={{ paddingRight: 15, paddingTop: 15 }} size={30} />
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity onPress={() => this.setState({ show: !this.state.show })}>
                                        <Icon name="chevron-circle-down" color='grey' style={{ paddingRight: 15, paddingTop: 15 }} size={30} />
                                    </TouchableOpacity>
                            }

                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start', alignContent: 'flex-start' }}>
                            <View>
                                <Text style={{ color: 'black', paddingTop: 10, paddingLeft: 10 }}>Total Fee</Text>
                                <Text style={{ color: 'grey', paddingTop: 5, paddingLeft: 10 }}>$1000.00</Text>
                            </View>
                            <View>
                                <Text style={{ color: 'black', paddingTop: 10, }}>Total Part Payment</Text>
                                <Text style={{ color: 'grey', paddingTop: 5 }}>4</Text>
                            </View>
                            <View>
                                <Text style={{ color: 'black', paddingTop: 10, paddingRight: 15 }}>Total Free Paid</Text>
                                <Text style={{ color: 'grey', paddingTop: 5 }}>$500.00</Text>
                            </View>
                        </View>

                        {this.state.show === true ?
                            <View>
                                <View style={{ width: wp('100%'), borderColor: 'grey', borderWidth: 1, marginTop: 10 }} />
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start', alignContent: 'flex-start' }}>
                                    <View style={{ marginLeft: 63 }} />
                                    <Text style={{ paddingTop: 15 }}>Part Payment 1</Text>
                                    <Text style={{ paddingTop: 15 }}>250.00$</Text>
                                    <View style={paymentsStyles.compliteView}>
                                        <Text style={{ color: 'white' }}>Completed</Text></View>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start', alignContent: 'flex-start' }}>
                                    <CheckBox
                                        checkedColor='#00a64f'
                                        checked={this.state.checked}
                                        onPress={() => this.setState({ checked: !this.state.checked })}
                                    />
                                    <Text style={{ paddingTop: 15 }}>Part Payment 1</Text>
                                    <Text style={{ paddingTop: 15 }}>250.00$</Text>
                                    <View style={paymentsStyles.paddingView}>
                                        <Text style={{ color: 'white' }}>Pending</Text></View>
                                </View>
                            </View>
                            : null}

                    </View>
                    {this.state.show === true ?
                        <View style={paymentsStyles.mainView}>
                            <View style={{ padding: 10, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <View>
                                    <Text>Course Fee</Text>
                                    <TextInput
                                        style={paymentsStyles.input}
                                        onChangeText={(text) => this.setState({ courseFee: text })}
                                        placeholder='$00.00'
                                        underlineColorAndroid="transparent"
                                        value={this.state.courseFee}
                                        keyboardType="number-pad"
                                    />
                                </View>
                                <View>
                                    <Text>Installment Premium</Text>
                                    <TextInput
                                        style={paymentsStyles.input}
                                        onChangeText={(text) => this.setState({ installment: text })}
                                        placeholder='$00.00'
                                        underlineColorAndroid="transparent"
                                        value={this.state.installment}
                                        keyboardType="number-pad"
                                    />
                                </View>

                            </View>
                            <View style={{ padding: 10, }}>
                                <Text>Total Amount</Text>
                                <TextInput
                                    style={paymentsStyles.fullinput}
                                    onChangeText={(text) => this.setState({ total: text })}
                                    placeholder='$00.00'
                                    underlineColorAndroid="transparent"
                                    value={this.state.total}
                                    keyboardType="number-pad"

                                />
                            </View>
                            <Ripple rippleColor="#99FF99" style={paymentsStyles.loginBtn} onPress={this.props.navigation.navigate('MakePayment')}>
                                <Text style={paymentsStyles.loginText}>Pay Now</Text>
                            </Ripple>

                        </View> : null}

                </ScrollView> */}
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    const { token } = state.auth;
    const { paymentStore } = state.payment;
    return {
        paymentData: paymentStore,
        token: token,
    };
}

const mapDispatchToProps = (dispatch) => ({
    stateDispatch: (PAGE, LIMIT, token) => dispatch(Actions.GetPaymentsList(PAGE, LIMIT, token))
});

export default connect(mapStateToProps, mapDispatchToProps)(PaymentsList);