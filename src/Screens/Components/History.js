import React, { Component } from 'react'
import { Text, View, ScrollView, FlatList } from 'react-native'
import Header from '../CommonComponents/Header';
import { historyStyles } from '../CommonComponents/css';
import { connect } from "react-redux";
import * as Actions from '../../Redux/Action/historyAction';
import { LIMIT, PAGE } from '../../Redux/const';
import Moment from 'react-moment';

export class History extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: [],
        }
        this.ListEmptyView = this.ListEmptyView.bind(this);
    }

    componentDidMount = async () => {
        await this.props.stateDispatch(PAGE, LIMIT, this.props.token);
        this.setState({ data: this.props.historyData.data });
    }


    ListEmptyView = () => {
        return (
            <View style={historyStyles.mainView}>
                <Text style={{ fontWeight: 'bold', textAlign: 'center' }}> Sorry, No Data Present... Try Again.</Text>
            </View>
        );
    }

    render() {
        return (
            <View style={historyStyles.container}>
                <Header title={'History'} />
                <FlatList
                    data={this.state.data}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) =>
                        <View key={item.id} style={historyStyles.mainView}>
                            <View style={historyStyles.subView}>
                                <Text style={historyStyles.txtTypeaction}>Action Type :</Text>
                                <Text style={historyStyles.txtSub}>{item.type}</Text>
                            </View>
                            <View style={historyStyles.subView}>
                                <Text style={historyStyles.txtTypeaction}>Timestamp :</Text>
                                <Moment style={historyStyles.txtTime} format='MMM D, YYYY | h:mm:ss A' element={Text} >{item.last_updated}</Moment>
                            </View>
                            <View style={historyStyles.subView}>
                                <Text style={historyStyles.txtTypeaction}>Action Details :</Text>
                                <Text style={historyStyles.txtDesc}>{item.user_agent}</Text>
                            </View>
                        </View>
                    }
                ListEmptyComponent={this.ListEmptyView}
                />
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    const { token } = state.auth;
    const { historyStore } = state.history;
    return {
        historyData: historyStore,
        token: token,
    };
}

const mapDispatchToProps = (dispatch) => ({
    stateDispatch: (PAGE, LIMIT, token) => dispatch(Actions.HistoryList(PAGE, LIMIT, token))
});

export default connect(mapStateToProps, mapDispatchToProps)(History);;