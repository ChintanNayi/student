import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  Animated,
  TouchableOpacity,
  Button,
  ScrollView,
} from "react-native";
import logo from "../../Assets/dsc-logo.png";
import { Avatar } from "react-native-elements";
import Icon from "react-native-vector-icons/Feather";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { Neomorph } from "react-native-neomorph-shadows";
import ViewOverflow from "react-native-view-overflow";
import { dashboardStyles } from "../CommonComponents/css";
import AsyncStorage from "@react-native-community/async-storage";

import Timesheets from "../../Assets/Timesheets.png";
import Feedback from "../../Assets/Feedback.png";
import History from "../../Assets/History.png";
import MakePayment from "../../Assets/MakePayment.png";
import PaymentList from "../../Assets/PaymentList.png";
import Profile from "../../Assets/Profile.png";
import { connect } from "react-redux";
import { USER_LOGOUT } from "../../Redux/actionType";

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showProfile: false,
      SlideInLeft: new Animated.Value(0),
      slideUpValue: new Animated.Value(0),
      fadeValue: new Animated.Value(0),
    };
    Animated.parallel([
      Animated.timing(this.state.SlideInLeft, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true,
      }),
      Animated.timing(this.state.fadeValue, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true,
      }),
      Animated.timing(this.state.slideUpValue, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true,
      }),
    ]).start();
    //commit
  }

  componentDidMount = async () => {
    // await this.props.token;
    // await this.props.user;
    // console.log("Dashboard Mount Function==>>", this.props.token)
  }

  logOut = async () => {
    this.setState({ showProfile: !this.state.showProfile })
    await this.props.stateDispatchLogout({ type: USER_LOGOUT },)
    AsyncStorage.clear();
    this.props.navigation.replace("LoginFlow");
  };

  render() {
    let { slideUpValue, fadeValue, SlideInLeft } = this.state;
    return (
      <View style={dashboardStyles.container}>
        <View style={dashboardStyles.head}>
          <Text style={dashboardStyles.title}>
            Blue Mountain Driving School
          </Text>
          <Image style={dashboardStyles.image} source={logo} />
        </View>

        <ScrollView style={{ left: '5%', width: "100%" }} showsVerticalScrollIndicator={false}>
          <View style={dashboardStyles.userView}>
            <Text style={dashboardStyles.user}>Smith D.Jeo</Text>
            <View style={dashboardStyles.imgView}>
              <Avatar
                containerStyle={{
                  alignSelf: "center",
                  backgroundColor: "grey",
                  marginTop: 7,
                }}
                rounded
                icon={{ name: "user-alt", type: "font-awesome-5" }}
                onAccessoryPress={() => Alert.alert("change avatar")}
                overlayContainerStyle={{ backgroundColor: "grey" }}
                size={25}
                onPress={() =>
                  this.setState({ showProfile: !this.state.showProfile })
                }
              />
              {this.state.showProfile === true ? (
                <View style={dashboardStyles.profileView}>
                  <TouchableOpacity
                    onPress={() => { this.setState({ showProfile: !this.state.showProfile }), this.props.navigation.navigate("Profile") }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Avatar
                        containerStyle={{
                          alignSelf: "center",
                          backgroundColor: "grey",
                        }}
                        rounded
                        icon={{ name: "user-alt", type: "font-awesome-5" }}
                        onAccessoryPress={() => Alert.alert("change avatar")}
                        overlayContainerStyle={{ backgroundColor: "grey" }}
                        size={15}
                        onPress={() => console.log("Works!")}
                      />
                      <Text style={{ color: "grey", paddingLeft: 5 }}>
                        Profile
                      </Text>
                    </View>
                  </TouchableOpacity>
                  <View style={{ borderWidth: 1, borderColor: "grey" }} />
                  <TouchableOpacity
                    onPress={() => this.logOut()}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Icon
                        name="log-out"
                        style={{ paddingTop: 3 }}
                        color="grey"
                        size={15}
                      />
                      <Text style={{ color: "grey", paddingLeft: 5 }}>
                        Log Out
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              ) : null}
            </View>
          </View>
          <ViewOverflow>
            <View style={dashboardStyles.mainView}>
              <View style={{ flexDirection: "column", marginBottom: 70 }}>
                <Animated.View
                  style={{
                    transform: [
                      {
                        translateX: slideUpValue.interpolate({
                          inputRange: [0, 1],
                          outputRange: [-600, 0],
                        }),
                      },
                    ],
                    flex: 1,
                    height: 200,
                    width: 150,
                    borderRadius: 12,
                    justifyContent: "center",
                  }}
                >
                  <View style={{ flexDirection: "row" }}>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate("Timesheet")
                      }
                    >
                      <Neomorph
                        style={dashboardStyles.itemView}
                      // darkShadowColor="#00a64f" // <- set this
                      // lightShadowColor="#feba24" // <- this
                      >
                        <View>
                          <Image
                            source={Timesheets}
                            style={{ marginTop: 35, alignSelf: "center" }} />
                          <Text
                            style={{
                              marginTop: 20,
                              color: "black",
                              fontSize: 15,
                            }}
                          >
                            Time Sheet
                          </Text>
                        </View>
                      </Neomorph>
                    </TouchableOpacity>

                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate("FeedBack")}
                    >
                      <Neomorph
                        style={dashboardStyles.itemView}
                      // darkShadowColor="#feba24" // <- set this
                      // lightShadowColor="#00a64f" // <- this
                      >
                        <View>
                          <Image
                            source={Feedback}
                            style={{ marginTop: 35, alignSelf: "center" }} />
                          <Text
                            style={{
                              marginTop: 20,
                              color: "black",
                              fontSize: 15,
                            }}
                          >
                            Feed Back
                          </Text>
                        </View>
                      </Neomorph>
                    </TouchableOpacity>
                  </View>
                </Animated.View>
                <Animated.View
                  style={{
                    transform: [
                      {
                        translateY: SlideInLeft.interpolate({
                          inputRange: [0, 1],
                          outputRange: [600, 0],
                        }),
                      },
                    ],
                    flex: 1,
                    height: 200,
                    width: 150,
                    borderRadius: 12,
                    justifyContent: "center",
                  }}
                >
                  <View style={{ flexDirection: "row" }}>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate("PaymentList")
                      }
                    >
                      <Neomorph
                        style={dashboardStyles.itemView}
                      // darkShadowColor="#feba24" // <- set this
                      // lightShadowColor="#00a64f" // <- this
                      >
                        <View>
                          <Image
                            source={PaymentList}
                            style={{ marginTop: 35, alignSelf: "center" }} />
                          <Text
                            style={{
                              marginTop: 20,
                              color: "black",
                              fontSize: 15,
                            }}
                          >
                            Payments List
                          </Text>
                        </View>
                      </Neomorph>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate("History")}
                    >
                      <Neomorph
                        style={dashboardStyles.itemView}
                      // darkShadowColor="#00a64f" // <- set this
                      // lightShadowColor="#feba24" // <- this
                      >
                        <View>
                          <Image
                            source={History}
                            style={{ marginTop: 35, alignSelf: "center" }} />
                          <Text
                            style={{
                              marginTop: 20,
                              color: "black",
                              fontSize: 15,
                            }}
                          >
                            History
                          </Text>
                        </View>
                      </Neomorph>
                    </TouchableOpacity>
                  </View>
                </Animated.View>
                <Animated.View
                  style={{
                    opacity: fadeValue,
                    flex: 1,
                    height: 200,
                    width: 150,
                    borderRadius: 12,
                    justifyContent: "center",
                  }}
                >
                  <View style={{ flexDirection: "row" }}>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate("MakePayment")
                      }
                    >
                      <Neomorph
                        style={dashboardStyles.itemView}
                      // darkShadowColor="#00a64f" // <- set this
                      // lightShadowColor="#feba24" // <- this
                      >
                        <View>
                          <Image
                            source={MakePayment}
                            style={{ marginTop: 35, alignSelf: "center" }} />
                          <Text
                            style={{
                              marginTop: 20,
                              color: "black",
                              fontSize: 15,
                            }}
                          >
                            Make Payments
                          </Text>
                        </View>
                      </Neomorph>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate("Profile")}
                    >
                      <Neomorph
                        style={dashboardStyles.itemView}
                      // darkShadowColor="#feba24" // <- set this
                      // lightShadowColor="#00a64f" // <- this
                      >
                        <View>
                          <Image
                            source={Profile}
                            style={{ marginTop: 35, alignSelf: "center" }} />
                          <Text
                            style={{
                              marginLeft: 6,
                              marginTop: 20,
                              color: "black",
                              fontSize: 15,
                            }}
                          >
                            Profile
                          </Text>
                        </View>
                      </Neomorph>
                    </TouchableOpacity>
                  </View>
                </Animated.View>
              </View>
            </View>
          </ViewOverflow>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const { token, user } = state.auth;
  return {
    token: token,
    user: user,
  };
}

const mapDispatchToProps = (dispatch) => ({
  stateDispatchLogout: ({ type: USER_LOGOUT }) => dispatch({ type: USER_LOGOUT }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);