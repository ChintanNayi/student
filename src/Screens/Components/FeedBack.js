import React, { Component } from 'react'
import { Text, View, ScrollView, FlatList } from 'react-native'
import Header from '../CommonComponents/Header'
import { feedBackstyles } from '../CommonComponents/css';
import { connect } from "react-redux";
import * as Actions from '../../Redux/Action/feedbackAction';
import { LIMIT, PAGE } from '../../Redux/const';
import Moment from 'react-moment';

export class FeedBack extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: [],
        }
        this.ListEmptyView = this.ListEmptyView.bind(this)
    }

    ListEmptyView = () => {
        return (
            <View style={feedBackstyles.mainView}>
                <Text style={{ fontWeight: 'bold', textAlign: 'center' }}> Sorry, No Data Present... Try Again.</Text>
            </View>
        );
    }

    componentDidMount = async () => {
        await this.props.stateDispatch(PAGE, LIMIT, this.props.token);
        this.setState({ data: this.props.feedbackData.data });
    }

    render() {
        return (
            <View style={feedBackstyles.container}>
                <Header title={'Feed Back'} />

                <FlatList
                    data={this.state.data}
                    ListEmptyComponent={this.ListEmptyView}
                    renderItem={({ item }) =>
                        <View key={item.id} style={feedBackstyles.mainView}>
                            <View style={feedBackstyles.subView}>
                                <Text style={feedBackstyles.headTxt}>Class Name</Text>
                                <Text style={feedBackstyles.txtClassName}>{item.course_name}</Text>
                            </View>
                            <View style={feedBackstyles.subView}>
                                <Text style={feedBackstyles.headTxt}>Timestamp</Text>
                                <Moment style={feedBackstyles.txtTime} format='MMM D, YYYY | h:mm:ss A' element={Text} >{item.date_time}</Moment>
                            </View>
                            <View style={feedBackstyles.subView}>
                                <Text style={feedBackstyles.headTxt}>Instructore Name</Text>
                                <Text style={feedBackstyles.txtName}>{item.instructor_name}</Text>
                            </View>
                            <View style={feedBackstyles.subView}>
                                <Text style={feedBackstyles.headTxt}>Message</Text>
                                <Text style={feedBackstyles.subTxt}>{item.message}</Text>
                            </View>
                        </View>
                    }
                />

                {/* <ScrollView showsVerticalScrollIndicator={false} style={{ marginTop: 35 }}>
                    {
                        this.state.data.map((item) => {
                            return (
                                
                                <View key={item.id} style={feedBackstyles.mainView}>
                                    <View style={feedBackstyles.subView}>
                                        <Text style={feedBackstyles.headTxt}>Class Name</Text>
                                        <Text style={feedBackstyles.txtClassName}>{item.course_name}</Text>
                                    </View>
                                    <View style={feedBackstyles.subView}>
                                        <Text style={feedBackstyles.headTxt}>Timestamp</Text>
                                        <Text style={feedBackstyles.txtTime}>{item.date_time}</Text>
                                    </View>
                                    <View style={feedBackstyles.subView}>
                                        <Text style={feedBackstyles.headTxt}>Instructore Name</Text>
                                        <Text style={feedBackstyles.txtName}>{item.instructor_name}</Text>
                                    </View>
                                    <View style={feedBackstyles.subView}>
                                        <Text style={feedBackstyles.headTxt}>Message</Text>
                                        <Text style={feedBackstyles.subTxt}>{item.message}</Text>
                                    </View>
                                </View>
                            )
                        })
                    }
                </ScrollView> */}
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    const { token } = state.auth;
    const { feedbackStore } = state.feedback;
    return {
        feedbackData: feedbackStore,
        token: token
    };
}

const mapDispatchToProps = (dispatch) => ({
    stateDispatch: (PAGE, LIMIT, token) => dispatch(Actions.FeedBackList(PAGE, LIMIT, token)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FeedBack);;