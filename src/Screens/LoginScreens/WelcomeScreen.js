import React, { Component } from "react";
import { Image, View, TouchableOpacity, Text } from "react-native";
import logo from "../../Assets/dsc-main-logo.png";
import WelcomeImg from "../../Assets/welcome.png";
import Ripple from "react-native-material-ripple";
import { WlcStyles } from "../CommonComponents/css";
import AsyncStorage from "@react-native-community/async-storage";

export default class WelcomeScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    // Start counting when the page is loaded
    this.timeoutHandle = setTimeout(() => {
      // Add your logic for the transition
      // AsyncStorage.getItem("isLogin").then((value) =>
      //   this.props.navigation.replace(value === null ? "LoginFlow" : "HomeScreen")
      // );
    }, 3000);
  }

  componentWillUnmount() {
    clearTimeout(this.timeoutHandle);
  }

  render() {
    return (
      <View style={WlcStyles.container}>
        <Image style={WlcStyles.image} source={logo} />
        <Image style={WlcStyles.mainImg} source={WelcomeImg} />

        <View style={WlcStyles.bottom}>
          <Text style={WlcStyles.textbottom}>
            @ 2021 Driving School Cloud. All rights reserved
          </Text>
        </View>
      </View>
    );
  }
}
