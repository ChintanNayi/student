import React, { Component } from 'react'
import { Button, View, Text, TextInput, TouchableOpacity, Image, ScrollView } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context';
import LOGO from '../../Assets/dsc-main-logo.png';
import { Input } from 'react-native-elements';
import Ripple from 'react-native-material-ripple';
import { fpwdstyles } from '../CommonComponents/css';
import { connect } from "react-redux";
import { SendOtpResetPassword } from "../../Redux/Action/loginAction";
import Toast from 'react-native-toast-message';
var { Platform } = React;

export class ForgetPassword extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: "",
            password: "",
            isValidEmail: true,
            forgetSendEmailState: ""
        }
    }
    //Toast Message
    showToast = () =>
        Toast.show({
            visibilityTime: 2000,
            position: 'top',
            text1: 'OTP Sent Successfully!',
            topOffset: Platform.OS === "ios" ? 40 : 5,
        });

    CallToAction = (text) => {
        const nav = this.props.navigation
        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (
            reg.test(this.state.email) === true
        ) {
            this.setState({
                email: text,
                isValidEmail: true,
            });
            let userObj = {
                email: this.state.email
            }
            //   await this.props.dispatchState(userObj);
            this.showToast();
            setTimeout(() => {
                this.setState({ loaded: false });
                nav.navigate("ForgotPasswordOTP");
            }, 1000);
        } else {
            this.setState({
                email: text,
                isValidEmail: false,
            });
            this.setState({ loaded: false });
        }

    }

    render() {
        return (
            <ScrollView>
                <Toast />
                <SafeAreaView style={fpwdstyles.container}>
                    <View style={{ alignItems: 'center' }}>
                        <Image style={fpwdstyles.image} source={LOGO} /></View>
                    <Text style={fpwdstyles.head}>Forgot Password</Text>
                    <Input
                        containerStyle={{ width: '85%' }}
                        style={{ fontSize: 14 }}
                        placeholder="Enter Your Email Address"
                        leftIcon={{ type: 'font-awesome', name: 'user-o', color: 'grey' }}
                        onChangeText={(text) => this.setState({ email: text })}
                        errorMessage={this.state.isValidEmail ? null : ('Please enter a valid username.')}
                    />
                    {/* {this.state.isValidEmail ? null : (
                        <View style={{ paddingRight: 188 }}>
                            <Text style={fpwdstyles.errorMsg}>
                                Please enter a valid username.
                            </Text>
                        </View>
                    )} */}

                    <Ripple rippleColor="#99FF99" rippleOpacity={0.9} onPress={() => this.CallToAction()} style={fpwdstyles.loginBtn}>
                        <Text style={fpwdstyles.loginText}>Call to Action</Text>
                    </Ripple>

                </SafeAreaView>
            </ScrollView>
        )
    }
}

const mapStateToProps = (state) => {
    const { sendforgototp } = state.auth;
    return { forgetSendEmailState: sendforgototp };
}

const mapDispatchToProps = (dispatch) => (
    {
        dispatchState: (userObj) => dispatch(SendOtpResetPassword(userObj))
    }
);

export default connect(mapStateToProps, mapDispatchToProps)(ForgetPassword);