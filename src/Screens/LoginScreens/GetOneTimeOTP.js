import React, { Component } from "react";
import { View, Text, TouchableOpacity, Image, ScrollView } from "react-native";
import LOGO from "../../Assets/dsc-main-logo.png";
import Ripple from "react-native-material-ripple";
import { GOTPstyles } from "../CommonComponents/css";
import { Spinner } from "native-base";
import { connect } from "react-redux";
import * as authAction from "../../Redux/Action/loginAction";
import Toast from 'react-native-toast-message';
var { Platform } = React;

export class GetOneTimeOTP extends Component {
  constructor(props) {
    super(props);

    this.state = {
      checked: "first",
      options: [
        {
          key: "email",
          text: "Email Id",
        },
        {
          key: "mobile",
          text: "Phone Number",
        },
      ],
      selectedOption: "email",
      otpState: ""
    };
    this.onSelect = this.onSelect.bind(this);
  }

  onSelect = (item) => {
    this.setState({ selectedOption: item });
    console.log("test", this.state.selectedOption, item);
    if (this.state.selectedOption === item.key) {
      this.setState({ selectedOption: null });
    } else {
      this.setState({ selectedOption: item.key });
    }
  };

  //Toast Message
  successToast = () =>
    Toast.show({
      visibilityTime: 2000,
      position: 'top',
      text1: 'OTP Sent Successfully!',
      topOffset: Platform.OS === "ios" ? 40 : 5,
    });

  errorToast = () =>
    Toast.show({
      type: 'error',
      visibilityTime: 3000,
      position: 'top',
      text1: 'OTP is invalid.',
      topOffset: Platform.OS === "ios" ? 40 : 5,
    })

  sendOTP = async () => {
    let userObj;
    this.setState({ loaded: true });
    if (this.state.selectedOption === "email") {
      userObj = {
        email: this.props.otpState.email,
      };
    }
    else {
      userObj = {
        mobile: this.props.otpState.mobile,
      };
    }

    let resData = await this.props.dispatchState(userObj, this.state.selectedOption);

    // console.log("resData-->>", this.state.selectedOption);

    if (!resData.error) {
      this.setState({ selectedOption: "email" });
      this.successToast();
      setTimeout(() => {
        this.setState({ loaded: false });
        this.props.navigation.navigate("EnterOTP");
      }, 1000);
      return;
    }
    this.errorToast();
    this.setState({ loaded: false });
  };

  render() {
    return (
      <ScrollView>
        <Toast />
        <View style={GOTPstyles.container}>
          <View style={{ alignItems: "center" }}>
            <Image style={GOTPstyles.image} source={LOGO} />
          </View>
          <Text style={GOTPstyles.head}>Get One-Time Code</Text>

          <View
            style={{ alignSelf: "flex-start", marginLeft: 60, marginTop: 80 }}
          >
            {this.state.options.map((item) => {
              return (
                <View key={item.key} style={GOTPstyles.buttonContainer}>
                  <TouchableOpacity
                    style={GOTPstyles.circle}
                    onPress={() => {
                      this.onSelect(item);
                    }}
                  >
                    {this.state.selectedOption === item.key && (
                      <View style={GOTPstyles.checkedCircle} />
                    )}
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      this.onSelect(item);
                    }}
                  >
                    <Text style={{ marginLeft: 20, color: "black" }}>
                      {item.text}
                    </Text>
                  </TouchableOpacity>
                </View>
              );
            })}
          </View>

          {this.state.loaded === true ? (
            <Spinner style={{ marginTop: 30 }} size={"lg"} color="#f0bf4c" />
          ) : (
            <Ripple
              rippleColor="#99FF99"
              rippleOpacity={0.9}
              onPress={() => this.sendOTP()}
              style={GOTPstyles.loginBtn}
            >
              <Text style={GOTPstyles.loginText}>Send</Text>
            </Ripple>
          )}
        </View>
      </ScrollView>
    );
  }
}
const mapStateToProps = (state) => {
  const { user } = state.auth;
  // console.log("USER DATA ON SEND OTP", user);
  return { otpState: user };
}

const mapDispatchToProps = (dispatch) => (
  {
    dispatchState: (userObj, type) => dispatch(authAction.SendOtp(userObj, type))
  }
);

export default connect(mapStateToProps, mapDispatchToProps)(GetOneTimeOTP);