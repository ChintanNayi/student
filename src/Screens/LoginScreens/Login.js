import React, { Component } from "react";
import {
  Button,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  SafeAreaView,
} from "react-native";
import { Input, Icon } from "react-native-elements";
import LOGO from "../../Assets/dsc-main-logo.png";
import * as Animatable from "react-native-animatable";
import Ripple from "react-native-material-ripple";
import { loginStyles } from "../CommonComponents/css";
import { Spinner } from "native-base";
import { connect } from "react-redux";
import * as authAction from '../../Redux/Action/loginAction';
import Toast from 'react-native-toast-message';
var { Platform } = React;

export class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      animation: "",
      isValidEmail: true,
      isValidPassword: true,
      isSecureEntry: true,
      loginState: ""
    };
  }

  successToast = () =>
    Toast.show({
      visibilityTime: 3000,
      position: 'top',
      text1: 'You have signed in successfully.',
      topOffset: Platform.OS === "ios" ? 40 : 5,
    });

  errorToast = () =>
    Toast.show({
      type: 'error',
      visibilityTime: 3000,
      position: 'top',
      text1: 'Invalid User.',
      topOffset: Platform.OS === "ios" ? 40 : 5,
    })

  SignIn = async () => {
    this.setState({ loaded: true });
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (
      reg.test(this.state.email) === true &&
      this.state.password.trim().length >= 6
    ) {
      this.setState({
        isValidEmail: true,
        isValidPassword: true,
      });
      this.setState({ loaded: true });
      let obj = {
        email: this.state.email,
        password: this.state.password
      }

      let resData = await this.props.stateDispatch(obj);
      // console.log("EMAIL AND PASS-->>", resData);

      if (!resData) {
        this.setState({ loaded: false });
        this.errorToast();
        return;
      }
      this.successToast();
      this.setState({ email: "", password: "" })
      setTimeout(() => {
        this.setState({ loaded: false });
        this.props.navigation.navigate("SendOTP");
      }, 1000);
    } else {
      this.setState({
        isValidEmail: false,
        isValidPassword: false,
      });
      this.setState({ loaded: false });
    }
  };

  render() {
    const MyCustomComponent =
      Animatable.createAnimatableComponent(TouchableOpacity);
    return (
      <ScrollView>
        <Toast />
        <SafeAreaView style={loginStyles.container}>
          <View style={{ alignItems: "center" }}>
            <Image style={loginStyles.image} source={LOGO} />
          </View>
          <Text style={loginStyles.head}>Sign In</Text>

          <Input
            containerStyle={{ width: '85%' }}
            style={{ fontSize: 14 }}
            placeholder="Enter User Name"
            leftIcon={{ type: 'font-awesome', name: 'user-o', color: 'grey' }}
            onChangeText={(text) => this.setState({ email: text })}
            errorMessage={this.state.isValidEmail ? null : ('Please enter a valid username.')}
          />
          <Input
            secureTextEntry={this.state.isSecureEntry}
            containerStyle={{ width: '85%' }}
            style={{ fontSize: 15, right: 20 }}
            placeholder="Enter Password"
            leftIcon={{ type: 'evilicon', name: 'unlock', size: 40, color: 'grey' }}
            leftIconContainerStyle={{ right: 10 }}
            rightIcon={
              <TouchableOpacity onPress={() => this.setState({ isSecureEntry: !this.state.isSecureEntry })}>
                <Icon name='eye' type='font-awesome' size={20} color='grey'>{this.state.isSecureEntry}</Icon>
              </TouchableOpacity>
            }
            onChangeText={(text) => this.setState({ password: text })}
            errorMessage={this.state.isValidPassword ? null : ('Password must be at least 8 characters.')}
          />
          <View style={{ flexDirection: "row" }}>
            <MyCustomComponent
              animation={this.state.animation}
              onPress={() => {
                this.props.navigation.navigate("ForgetPassword"),
                  this.setState({ animation: "wobble" });
              }}
              style={loginStyles.bottom}
            >
              <Text style={loginStyles.forgot_button}>Forgot Password?</Text>
            </MyCustomComponent>
          </View>
          {this.state.loaded === true ? (
            <Spinner style={{ marginTop: 30 }} size={"lg"} color="#f0bf4c" />
          ) : (
            <Ripple
              rippleColor="#99FF99"
              rippleOpacity={0.9}
              onPress={() => this.SignIn()}
              style={loginStyles.loginBtn}
            >
              <Text style={loginStyles.loginText}>Sign In</Text>
            </Ripple>
          )}
        </SafeAreaView>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => {
  const { login } = state.auth;
  return { loginState: login };
}

const mapDispatchToProps = (dispatch) => ({
  stateDispatch: (data) => dispatch(authAction.UserLogin(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);;
