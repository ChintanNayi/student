import React, { Component } from "react";
import { Text, View, Image, TouchableOpacity, ScrollView } from "react-native";
import LOGO from "../../Assets/dsc-main-logo.png";
import Ripple from "react-native-material-ripple";
import OTPInputView from "@twotalltotems/react-native-otp-input";
import { EntStyles } from "../../Screens/CommonComponents/css";
import { Spinner } from "native-base";
import { Icon } from 'react-native-elements';
import { connect } from "react-redux";
import { VerifyOtpResetPassword } from "../../Redux/Action/loginAction";
import Toast from 'react-native-toast-message';
var { Platform } = React;

export class ForgotPasswordOTP extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isVisible: false,
            eff: false,
            timer: null,
            counter: 59,
            phone: "+919924460329",
            confirmResult: null,
            verificationCode: "",
            userId: "",
        };
    }

    componentDidMount() {
        this.startTimer();
    }

     //Toast Message
     showToast = () =>
     Toast.show({
         visibilityTime: 2000,
         position: 'top',
         text1: 'OTP Sent Successfully!',
         topOffset: Platform.OS === "ios" ? 40 : 5,
     });

    handleVerifyCode = () => {
        this.setState({ loaded: true });
        let userObj = {
            email:this.state.email,
            otp:this.state.verificationCode
          }
          // await this.props.dispatchState(userObj);
          this.showToast();
        setTimeout(() => {
            this.setState({ loaded: false });
        }, 2500);
        this.setState({ eff: true, isVisible: true });
        this.timeoutHandle = setTimeout(() => {
            // Add your logic for the transition
            this.setState({ isVisible: false, eff: false });
            this.props.navigation.navigate("NewPassword");
        }, 1000);
    };

    startTimer = () => {
        this.setState({
            show: false,
        });
        let timer = setInterval(this.manageTimer, 1000);
        this.setState({ timer });
    };

    manageTimer = () => {
        var states = this.state;

        if (states.counter === 0) {
            clearInterval(this.state.timer);
            this.setState({
                counter: 59,
                show: true,
            });
        } else {
            this.setState({
                counter: this.state.counter - 1,
            });
        }
    };

    componentWillUnmount() {
        clearInterval(this.state.timer);
    }

    render() {
        return (
            <ScrollView>
                <Toast />
                <View style={EntStyles.container}>
                    <View style={{ alignItems: "center" }}>
                        <Image style={EntStyles.image} source={LOGO} />
                    </View>
                    <Text style={EntStyles.head}>Please Enter The 6-Digit Code</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Icon containerStyle={{ top: 11, paddingRight: 5 }} name='check' type='font-awesome' size={17} color='#007419'></Icon>
                        <Text style={EntStyles.subHead}>Check your Email for the OTP</Text>
                    </View>
                    <OTPInputView
                        autoFocusOnLoad
                        onCodeChanged={(code) => {
                            this.setState({ verificationCode: code });
                        }}
                        placeholderCharacter="0"
                        placeholderTextColor="grey"
                        style={{ width: "70%", height: 50 }}
                        pinCount={6}
                        codeInputFieldStyle={EntStyles.underlineStyleBase}
                        codeInputHighlightStyle={EntStyles.underlineStyleHighLighted}
                    />

                    {this.state.loaded === true ? (
                        <Spinner style={{ marginTop: 30 }} size={"lg"} color="#f0bf4c" />
                    ) : (
                        <Ripple
                            rippleColor="#99FF99"
                            rippleOpacity={0.9}
                            onPress={() => this.handleVerifyCode()}
                            style={EntStyles.loginBtn}
                        >
                            <Text style={EntStyles.loginText}>CONTINUE</Text>
                        </Ripple>
                    )}

                    {this.state.show === true ? (
                        <View style={EntStyles.timerView}>
                            <Text style={{ textAlign: "center", marginRight: 10 }}>
                                00:00
                            </Text>
                            <TouchableOpacity onPress={() => this.startTimer()}>
                                <Text style={EntStyles.resend}>Resend Code</Text>
                            </TouchableOpacity>
                        </View>
                    ) : (
                        <View style={EntStyles.timerView}>
                            <Text style={{ textAlign: "center", marginRight: 10 }}>
                                00:{this.state.counter}
                            </Text>
                            <Text style={EntStyles.resend1}>Resend Code</Text>
                        </View>
                    )}
                </View>
            </ScrollView>
        );
    }
}

const mapStateToProps = (state) => {
    const { verifyforgototp } = state.loginReducer;
    return { verifyForgotOtpState: verifyforgototp };
  }
  
  const mapDispatchToProps = (dispatch) => (
    {
      dispatchState: (userObj) => dispatch(VerifyOtpResetPassword(userObj))
    }
  );
  
  export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordOTP);