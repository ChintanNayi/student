import React, { Component } from "react";
import { Text, View, Image, TouchableOpacity, ScrollView } from "react-native";
import LOGO from "../../Assets/dsc-main-logo.png";
import Ripple from "react-native-material-ripple";
import OTPInputView from "@twotalltotems/react-native-otp-input";
import { EOTPstyles } from "../CommonComponents/css";
import AsyncStorage from "@react-native-community/async-storage";
import { Spinner } from "native-base";
import { connect } from "react-redux";
import * as authAction from '../../Redux/Action/loginAction';
import Toast from 'react-native-toast-message';
var { Platform } = React;

export class EnterOTP extends Component {
  constructor(props) {
    super(props);
    this.state = {
      timer: null,
      counter: 59,
      confirmResult: null,
      verificationCode: "",
    };
  }

  componentDidMount() {
    this.startTimer();
  }
  startTimer = () => {
    this.setState({
      show: false,
    });
    let timer = setInterval(this.manageTimer, 1000);
    this.setState({ timer });
  }

  successToast = () =>
    Toast.show({
      visibilityTime: 3000,
      position: 'top',
      text1: 'You have signed in successfully.',
      topOffset: Platform.OS === "ios" ? 40 : 5,
    });

  errorToast = () =>
    Toast.show({
      type: 'error',
      visibilityTime: 3000,
      position: 'top',
      text1: 'OTP is invalid.',
      topOffset: Platform.OS === "ios" ? 40 : 5,
    })

  verifyOTPCode = async () => {
    let userObj;
    let type = this.props.verifyState.setectedValue ?
      this.props.verifyState.setectedValue : "email";

    this.setState({ loaded: true });
    if (type === "email") {
      userObj = {
        email: this.props.verifyState.email,
        otp: this.state.verificationCode
      };
    } else {
      userObj = {
        email: this.props.verifyState.mobile,
        otp: this.state.verificationCode
      };
    }
    let resData = await this.props.stateDispatchOTP(userObj);
    // console.log("ENTER OTP ==>>", userObj);

    if (!resData) {
      this.setState({ loaded: false });
      this.errorToast();
      return;
    }
    this.setState({ verificationCode: "" })
    this.successToast();
    setTimeout(() => {
      this.setState({ loaded: false });
      this.props.navigation.navigate("HomeScreen");
    }, 1000);
    // AsyncStorage.setItem("isLogin", "login");
  };

  resendOTP = async () => {
    let resendOTPObj;
    let resendType = this.props.verifyState.setectedValue ?
      this.props.verifyState.setectedValue : "email";

    if (resendType === "email") {
      resendOTPObj = {
        email: this.props.verifyState.email,
      }
    } else {
      resendOTPObj = {
        mobile: this.props.verifyState.mobile,
      }
    }
    let responseData = await this.props.stateDispatchResendOTP(resendOTPObj, resendType);
    if (!responseData) {
      this.setState({ loaded: false });
      return;
    }
    this.successToast();
    this.setState({ loaded: false });
  };

  manageTimer = () => {
    var states = this.state;

    if (states.counter === 0) {
      clearInterval(this.state.timer);
      this.setState({
        counter: 59,
        show: true,
      });
    } else {
      this.setState({
        counter: this.state.counter - 1,
      });
    }
  };

  componentWillUnmount() {
    clearInterval(this.state.timer);
  }

  render() {
    return (
      <ScrollView>
        <Toast />
        <View style={EOTPstyles.container}>
          <View style={{ alignItems: "center" }}>
            <Image style={EOTPstyles.image} source={LOGO} />
          </View>
          <Text style={EOTPstyles.head}>Get Your One-Time Code</Text>
          <OTPInputView
            autoFocusOnLoad
            onCodeChanged={(code) => {
              this.setState({ verificationCode: code });
            }}
            placeholderCharacter="0"
            placeholderTextColor="grey"
            style={{ width: "70%", height: 50, marginTop: '12%' }}
            pinCount={6}
            codeInputFieldStyle={EOTPstyles.underlineStyleBase}
            codeInputHighlightStyle={EOTPstyles.underlineStyleHighLighted}
          />

          {this.state.loaded === true ? (
            <Spinner style={{ marginTop: 30 }} size={"lg"} color="#f0bf4c" />
          ) : (
            <Ripple
              rippleColor="#99FF99"
              rippleOpacity={0.9}
              onPress={() => this.verifyOTPCode()}
              style={EOTPstyles.loginBtn}
            >
              <Text style={EOTPstyles.loginText}>CONTINUE</Text>
            </Ripple>
          )}

          {this.state.show === true ? (
            <View style={EOTPstyles.timerView}>
              <Text style={{ textAlign: "center", marginRight: 10 }}>
                00:00
              </Text>
              <TouchableOpacity onPress={() => this.resendOTP()}>
                <Text style={EOTPstyles.resend}>Resend Code</Text>
              </TouchableOpacity>
            </View>
          ) : (
            <View style={EOTPstyles.timerView}>
              <Text style={{ textAlign: "center", marginRight: 10 }}>
                00:{this.state.counter}
              </Text>
              <Text style={EOTPstyles.resend1}>Resend Code</Text>
            </View>
          )}
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => {
  const { user } = state.auth;
  // console.log("ENTER OTP mapStateTO -->>", user);
  return { verifyState: user };
}

const mapDispatchToProps = (dispatch) => ({
  stateDispatchOTP: (data) => dispatch(authAction.verifyOTP(data)),
  stateDispatchResendOTP: (data, type) => dispatch(authAction.resendOTP(data, type)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EnterOTP);;
