import { 
    LIST_OF_TIME_SHEET,
    LIST_OF_TIME_SHEET_ERROR,
} from '../actionType';

const initialState = {
   timesheet:null
}

const TimeSheetStore = (state = initialState, action) => {
    switch (action.type) {
        case LIST_OF_TIME_SHEET: {
            return {
                ...state,
                timesheet: action.payload
            };
        };
        case LIST_OF_TIME_SHEET_ERROR: {
            return {
                ...state,
                timesheet: null
            };
        };
        default:
            return state;
    }
}

export default TimeSheetStore;