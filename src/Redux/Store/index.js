import { combineReducers, applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunkMiddleware from 'redux-thunk';

import loginStore from "../Store/loginStore";
import TimeSheetStore from "./timeSheetStore";
import ProfileStore from "./profileStore";
import MakePaymentStore from "./makepaymentStore";
import PaymentStore from "./paymentStore";
import FeedBackStore from "./feedbackStore";
import HistoryStore from "./historyStore";

const composedEnhancer = composeWithDevTools(applyMiddleware(thunkMiddleware));

const rootReducer = combineReducers({
    auth: loginStore,
    timesheetReducer: TimeSheetStore,
    profileReducer: ProfileStore,
    makePaymentReducer: MakePaymentStore,
    payment: PaymentStore,
    feedback: FeedBackStore,
    history: HistoryStore
});

const configureStore = () => createStore(rootReducer, composedEnhancer);

export default configureStore;