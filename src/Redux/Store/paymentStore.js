import {
    ADD_PAYMENT,
    ADD_PAYMENT_ERROR,
    LIST_OF_PAYMENT,
    LIST_OF_PAYMENT_ERROR
} from '../actionType';

const initialState = {
    paymentStore: null,
    newpaymentStore: null
}

const PaymentStore = (state = initialState, action) => {
    switch (action.type) {
        case LIST_OF_PAYMENT: {
            return {
                ...state,
                paymentStore: action.payload,
                newpayment: null
            };
        };
        case LIST_OF_PAYMENT_ERROR: {
            return {
                ...state,
                paymentStore: null,
                newpayment: null
            };
        };
        case ADD_PAYMENT: {
            return {
                ...state,
                newpaymentStore: action.payload,
            };
        };
        case ADD_PAYMENT_ERROR: {
            return {
                ...state,
                newpaymentStore: null
            };
        };
        default:
            return state;
    }
}

export default PaymentStore;