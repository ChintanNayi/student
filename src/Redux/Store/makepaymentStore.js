import { 
  MAKE_PAYMENT,
  MAKE_PAYMENT_ERROR
 } from '../actionType';
 
 const initialState = {
   makePaymnent: null,
 }
 
 const MakePaymentStore = (state = initialState, action) => {
   switch (action.type) {
       case MAKE_PAYMENT: {
           return {
               ...state,
               makePaymnent: action.payload,
           };
       };
       case MAKE_PAYMENT_ERROR: {
           return {
               ...state,
               makePaymnent: null,
           };
       };
       default:
           return state;
   }
 }
 
 export default MakePaymentStore;