import AsyncStorage from '@react-native-community/async-storage';
import {
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    SELECT_OTP_PLATFORM,
    SEND_OTP,
    SEND_OTP_ERROR,
    VERIFY_OTP,
    VERIFY_OTP_ERROR,
    FORGOT_PASSWORD_SEND_OTP_EMAIL,
    FORGOT_PASSWORD_SEND_OTP_EMAIL_ERROR,
    USER_LOGOUT
} from '../actionType';

const userData = AsyncStorage.getItem("user");
const isAuthenticatedData = AsyncStorage.getItem("isAuthenticated");
const isVerifiedData = AsyncStorage.getItem("isVerified");

const initialState = {
    token: AsyncStorage.getItem("token") || "",
    isAuthenticated: isAuthenticatedData ? isAuthenticatedData : false,
    loading: true,
    user: userData ? userData : null,
    isVerified: isVerifiedData ? isVerifiedData : false,
    setectedValue: AsyncStorage.getItem("setectedValue") || "email",
}

const LoginStore = (state = initialState, action) => {
    const { type, payload, otpType } = action;

    switch (type) {
        case LOGIN_SUCCESS: {
            AsyncStorage.setItem("token", payload.access_token);
            AsyncStorage.setItem("user", JSON.stringify(payload.data));
            AsyncStorage.setItem("isAuthenticated", "false");
            AsyncStorage.setItem("isVerified", "false");
            return {
                ...state,
                token: payload.access_token,
                isAuthenticated: false,
                loading: false,
                user: payload.data,
                isVerified: false,
                setectedValue: null,
            };
        };
        case LOGIN_FAIL: {
            AsyncStorage.clear();
            return {
                ...state,
                token: null,
                isAuthenticated: false,
                loading: false,
                user: {},
                setectedValue: null,
            };
        };
        case SELECT_OTP_PLATFORM: {
            AsyncStorage.setItem("setectedValue", payload);
            return {
                ...state,
                selectedValue: payload,
            };
        }
        case SEND_OTP: {
            AsyncStorage.setItem("isAuthenticated", "true");
            // AsyncStorage.setItem("setectedValue", otpType);
            return {
                ...state,
                isAuthenticated: true,
                selectedValue: otpType,
            };
        };
        case SEND_OTP_ERROR: {
            return {
                ...state,
                otp: null,
                verify: null,
                sendforgototp: null
            };
        };
        case VERIFY_OTP: {
            AsyncStorage.setItem("isVerified", "true");
            return {
                ...state,
                isVerified: true,
            };
        };
        case VERIFY_OTP_ERROR: {
            return {
                ...state,
                verify: null,
                sendforgototp: null
            };
        };
        case FORGOT_PASSWORD_SEND_OTP_EMAIL: {
            return {
                ...state,
                sendforgototp: action.payload
            };
        };
        case FORGOT_PASSWORD_SEND_OTP_EMAIL_ERROR: {
            return {
                ...state,
                sendforgototp: null
            };
        };
        case USER_LOGOUT: {
            AsyncStorage.clear();
            return {
                ...state,
                token: null,
                isAuthenticated: false,
                loading: false,
                user: null,
                setectedValue: null,
            }
        }
        default:
            return state;
    }
}

export default LoginStore;