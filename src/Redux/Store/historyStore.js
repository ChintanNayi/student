import { 
  LIST_OF_HISTORY,
  LIST_OF_HISTORY_ERROR,
} from '../actionType';

const initialState = {
    historyStore:null
}

const HistoryStore = (state = initialState, action) => {
  switch (action.type) {
      case LIST_OF_HISTORY: {
          return {
              ...state,
              historyStore: action.payload
          };
      };
      case LIST_OF_HISTORY_ERROR: {
          return {
              ...state,
              historyStore: null
          };
      };
      default:
          return state;
  }
}

export default HistoryStore;