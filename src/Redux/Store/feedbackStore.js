import {
    LIST_OF_FEED_BACK,
    LIST_OF_FEED_BACK_ERROR,
} from '../actionType';

const initialState = {
    feedbackStore: null
}

const FeedBackStore = (state = initialState, action) => {
    const { type, payload } = action;
    switch (type) {
        case LIST_OF_FEED_BACK: {
            return {
                ...state,
                feedbackStore: payload
            };
        };
        case LIST_OF_FEED_BACK_ERROR: {
            return {
                ...state,
                feedbackStore: null
            };
        };
        default:
            return state;
    }
}

export default FeedBackStore;