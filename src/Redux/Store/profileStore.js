import { 
 PROFILE_DATA,
 PROFILE_DATA_ERROR
} from '../actionType';

const initialState = {
  profile: null,
}

const ProfileStore = (state = initialState, action) => {
  switch (action.type) {
      case PROFILE_DATA: {
          return {
              ...state,
              profile: action.payload,
          };
      };
      case PROFILE_DATA_ERROR: {
          return {
              ...state,
              profile: null,
          };
      };
      default:
          return state;
  }
}

export default ProfileStore;