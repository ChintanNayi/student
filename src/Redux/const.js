import API_END_POINT from '../../EndPoint';

export const config = {
  headers: {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
  },
};

export const LIMIT = 10;
export const PAGE = 1;
export const CODE_LIMIT = 4;

//LOGIN-SEND OTP-RESEND OTP-VERIFY OTP
export const STUDENT_LOGIN_API = API_END_POINT + "/auth/student/signin";

export const STUDENT_SEND_OTP_API = API_END_POINT + "/auth/student/signin/otp/email/send";
export const STUDENT_VERIFY_OTP_API = API_END_POINT + "/auth/student/signin/otp/email/verify";
export const STUDENT_RESEND_LOGIN_OTP_API = API_END_POINT + "/auth/student/signin/otp/email/resend";

export const RESET_PASSWORD_SEND_OTP_EMAIL_API = API_END_POINT + "/auth/student/signin/otp/email/send";
export const RESET_PASSWORD_API = API_END_POINT + "/auth/student/signin/otp/email/verify";
export const RESET_PASSWORD_VERIFY_OTP_API = API_END_POINT + "/auth/student/signin/otp/email/resend";

export const STUDENT_SEND_LOGIN_OTP_API_PHONE = API_END_POINT + "/auth/student/signin/otp/phone/send";
export const STUDENT_VERIFY_LOGIN_OTP_API_PHONE = API_END_POINT + "/auth/student/signin/otp/phone/verify";
export const STUDENT_RESEND_LOGIN_OTP_API_PHONE = API_END_POINT + "/auth/user/signin/otp/phone/resend";

export const GET_LIST_OF_TIME_SHEET = API_END_POINT + "";

export const GET_FEED_BACK_API = API_END_POINT + "/feedback/list/me";

export const GET_HISTORY_API = API_END_POINT + "/loginAttempts/history";

export const MAKE_PAYMENT_API = API_END_POINT + "";

export const GET_PAYMENT_LIST_API = API_END_POINT + "/payment/student/list/me";
export const ADD_PAYMENT_API = API_END_POINT + "";

export const PROFILE_API = API_END_POINT + "/student/me";