import axios from "axios";
import {
  MAKE_PAYMENT,
  MAKE_PAYMENT_ERROR
} from "../actionType";

import {
  MAKE_PAYMENT_API,
  config
} from "../const"

export const AddPayment = (userObj) => {
  const body = JSON.stringify(userObj);
  return (dispatch) => {
      axios.post(MAKE_PAYMENT_API,body,config)
          .then(response => {
              // console.log(response);
              dispatch({
                  type: MAKE_PAYMENT,
                  payload: response.data,
              });
              return response;
          })
          .catch(error => {
              console.log(error);
              dispatch({
                  type: MAKE_PAYMENT_ERROR,
                  payload: error,
              });
          });
  }
}
