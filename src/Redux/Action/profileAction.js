import axios from "axios";
import {
  PROFILE_DATA,
  PROFILE_DATA_ERROR,
  USER_PROFILE_DATA,
  USER_PROFILE_DATA_ERROR
} from "../actionType";

import {
  PROFILE_API,
  config
} from "../const"

export const ProfileData = (userObj) => {
  const body = JSON.stringify(userObj);
  return (dispatch) => {
      axios.post(PROFILE_API,body,config)
          .then(response => {
              // console.log(response);
              dispatch({
                  type: PROFILE_DATA,
                  payload: response.data,
              });
              return response;
          })
          .catch(error => {
              console.log(error);
              dispatch({
                  type: PROFILE_DATA_ERROR,
                  payload: error,
              });
          });
  }
}

export const UserDetails = (token) => {
  console.log(token)
  const header = {
      headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "multipart/form-data",
          "Access-Control-Allow-Origin": "*",
      },
  };
  return async (dispatch) => {
      return await axios
          .get(`${PROFILE_API}`, header)
          .then(response => {
              // console.log("FEEBACK ACTION ==>>", response);
              dispatch({
                  type: USER_PROFILE_DATA,
                  payload: response.data,
              });
              return response;
          })
          .catch(error => {
              console.log(error);
              dispatch({
                  type: USER_PROFILE_DATA_ERROR,
                  payload: error,
              });
          });
  }
}