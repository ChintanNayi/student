import axios from "axios";
import {
    LIST_OF_HISTORY,
    LIST_OF_HISTORY_ERROR
} from "../actionType";
import {
    config,
    GET_HISTORY_API
} from "../const"

export const HistoryList = (page, limit, token) => {
    const header = {
        headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "multipart/form-data",
            "Access-Control-Allow-Origin": "*",
        },
    };
    return (dispatch) => {
        axios.get(`${GET_HISTORY_API}?page=${page}&limit=${limit}`, header)
            .then(response => {
                // console.log(response);
                dispatch({
                    type: LIST_OF_HISTORY,
                    payload: response.data,
                });
                return response;
            })
            .catch(error => {
                console.log(error);
                dispatch({
                    type: LIST_OF_HISTORY_ERROR,
                    payload: error,
                });
            });
    }
}