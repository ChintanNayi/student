import axios from "axios";
import { 
    LIST_OF_TIME_SHEET,
    LIST_OF_TIME_SHEET_ERROR
    
} from "../actionType";
import {
    GET_LIST_OF_TIME_SHEET,
    config
} from "../const"

export const TimeSheetList = () => {
    return (dispatch) => {
        axios.get(GET_LIST_OF_TIME_SHEET,config)
            .then(response => {
                // console.log(response);
                dispatch({
                    type: LIST_OF_TIME_SHEET,
                    payload: response.data,
                });
                return response;
            })
            .catch(error => {
                console.log(error);
                dispatch({
                    type: LIST_OF_TIME_SHEET_ERROR,
                    payload: error,
                });
            });
    }
}