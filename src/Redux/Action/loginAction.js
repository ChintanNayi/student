import AsyncStorage from "@react-native-community/async-storage";
import axios from "axios";
import { Toast } from "native-base";
import {
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    SEND_OTP,
    SEND_OTP_ERROR,
    SELECT_OTP_PLATFORM,
    VERIFY_OTP,
    VERIFY_OTP_ERROR,
    FORGOT_PASSWORD_SEND_OTP_EMAIL,
    FORGOT_PASSWORD_SEND_OTP_EMAIL_ERROR,
    VERIFY_OTP_RESET_PASSWORD,
    VERIFY_OTP_RESET_PASSWORD_ERROR,
    ENTER_NEW_PASSWORD,
    ENTER_NEW_PASSWORD_ERROR

} from "../actionType";
import {
    STUDENT_LOGIN_API,
    STUDENT_SEND_OTP_API,
    STUDENT_SEND_LOGIN_OTP_API_PHONE,
    STUDENT_VERIFY_OTP_API,
    STUDENT_RESEND_LOGIN_OTP_API,
    STUDENT_RESEND_LOGIN_OTP_API_PHONE,
    RESET_PASSWORD_SEND_OTP_EMAIL_API,
    RESET_PASSWORD_VERIFY_OTP_API,
    RESET_PASSWORD_API,
    config,
} from "../const"

const token = AsyncStorage.getItem("token");
const header = {
    headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "multipart/form-data",
        "Access-Control-Allow-Origin": "*",
    },
};

export const UserLogin = (userObj) => {
    return async (dispatch) => {
        const body = JSON.stringify(userObj);
        // console.log("Action Data-->>", body);
        return await axios
            .post(STUDENT_LOGIN_API, body, config)
            .then(response => {
                // console.log(response);
                dispatch({
                    type: LOGIN_SUCCESS,
                    payload: response.data,
                });
                return response;
            })
            .catch(error => {
                console.log(error);
                dispatch({ type: LOGIN_FAIL });
            });
    };
};


export const SendOtp = (userObj, type) => {
    return async (dispatch) => {
        const API_URL = type === "email"
            ? STUDENT_SEND_OTP_API
            : STUDENT_SEND_LOGIN_OTP_API_PHONE;
        // const body = JSON.stringify(userObj);
        return await axios
            .post(API_URL, userObj)
            .then(response => {
                // console.log("SEND OTP ACTION RESPONSE-->>", response);
                dispatch({
                    type: SEND_OTP,
                    payload: response.data,
                    otpType: type,
                });
                return response;
            })
            .catch(error => {
                console.log(error);
                dispatch({
                    type: SEND_OTP_ERROR,
                    payload: error,
                });
            });
    };
}

export const updateOtpPlatFormValue = () => {
    return async (dispatch) => {
        dispatch({
            type: SELECT_OTP_PLATFORM,
            payload: otpType,
        });
    };
};

export const verifyOTP = (userObj) => {
    return async (dispatch) => {
        const body = JSON.stringify(userObj);
        return await axios
            .post(STUDENT_VERIFY_OTP_API, body, config)
            .then(response => {
                // console.log(response);
                dispatch({
                    type: VERIFY_OTP,
                    payload: response.data,
                });
                return response;
            })
            .catch(error => {
                console.log(error);
                dispatch({
                    type: VERIFY_OTP_ERROR,
                    payload: error,
                });
            });
    };
};

export const resendOTP = (userObj, type) => {
    return async (dispatch) => {
        const API_URL = type === "email"
            ? STUDENT_RESEND_LOGIN_OTP_API
            : STUDENT_RESEND_LOGIN_OTP_API_PHONE;
        // const body = JSON.stringify(userObj);
        return await axios
            .post(API_URL, userObj)
            .then(response => {
                // console.log("RESEND OTP ACTION RESPONSE-->>", response);
                dispatch({
                    type: SEND_OTP,
                    payload: response.data,
                });
                return response;
            })
            .catch(error => {
                console.log(error);
                dispatch({
                    type: SEND_OTP_ERROR,
                    payload: error,
                });
            });
    };
}

export const SendOtpResetPassword = (userObj) => {
    const body = JSON.stringify(userObj);
    return (dispatch) => {
        axios.post(RESET_PASSWORD_SEND_OTP_EMAIL_API, body, config)
            .then(response => {
                // console.log(response);
                dispatch({
                    type: FORGOT_PASSWORD_SEND_OTP_EMAIL,
                    payload: response.data,
                });
                return response;
            })
            .catch(error => {
                console.log(error);
                dispatch({
                    type: FORGOT_PASSWORD_SEND_OTP_EMAIL_ERROR,
                    payload: error,
                });
            });
    }
}

export const VerifyOtpResetPassword = (userObj) => {
    const body = JSON.stringify(userObj);
    return (dispatch) => {
        axios.post(RESET_PASSWORD_VERIFY_OTP_API, body, config)
            .then(response => {
                // console.log(response);
                dispatch({
                    type: VERIFY_OTP_RESET_PASSWORD,
                    payload: response.data,
                });
                return response;
            })
            .catch(error => {
                console.log(error);
                dispatch({
                    type: VERIFY_OTP_RESET_PASSWORD_ERROR,
                    payload: error,
                });
            });
    }
}

export const ChangePassword = (userObj) => {
    const body = JSON.stringify(userObj);
    return (dispatch) => {
        axios.post(RESET_PASSWORD_API, body, config)
            .then(response => {
                // console.log(response);
                dispatch({
                    type: ENTER_NEW_PASSWORD,
                    payload: response.data,
                });
                return response;
            })
            .catch(error => {
                console.log(error);
                dispatch({
                    type: ENTER_NEW_PASSWORD_ERROR,
                    payload: error,
                });
            });
    }
}