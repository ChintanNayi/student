import axios from "axios";
import {
    LIST_OF_FEED_BACK,
    LIST_OF_FEED_BACK_ERROR
} from "../actionType";
import {
    config,
    GET_FEED_BACK_API,
} from "../const"

export const FeedBackList = (page, limit, token) => {
    const header = {
        headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "multipart/form-data",
            "Access-Control-Allow-Origin": "*",
        },
    };
    return async (dispatch) => {
        return await axios
            .get(`${GET_FEED_BACK_API}?page=${page}&limit=${limit}`, header)
            .then(response => {
                // console.log("FEEBACK ACTION ==>>", response);
                dispatch({
                    type: LIST_OF_FEED_BACK,
                    payload: response.data,
                });
                return response;
            })
            .catch(error => {
                console.log(error);
                dispatch({
                    type: LIST_OF_FEED_BACK_ERROR,
                    payload: error,
                });
            });
    }
}