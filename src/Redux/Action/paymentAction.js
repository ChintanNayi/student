import axios from "axios";
import {
    ADD_PAYMENT,
    ADD_PAYMENT_ERROR,
    LIST_OF_PAYMENT,
    LIST_OF_PAYMENT_ERROR
} from "../actionType";

import {
    GET_PAYMENT_LIST_API,
    config,
    ADD_PAYMENT_API
} from "../const"

export const GetPaymentsList = (page, limit, token) => {
    const header = {
        headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "multipart/form-data",
            "Access-Control-Allow-Origin": "*",
        },
    };
    return (dispatch) => {
        axios.get(`${GET_PAYMENT_LIST_API}?page=${page}&limit=${limit}`, header)
            .then(response => {
                console.log(response);
                dispatch({
                    type: LIST_OF_PAYMENT,
                    payload: response.data,
                });
                return response;
            })
            .catch(error => {
                console.log(error);
                dispatch({
                    type: LIST_OF_PAYMENT_ERROR,
                    payload: error,
                });
            });
    }
}

export const AddPayment = (userObj) => {
    const body = JSON.stringify(userObj);
    return (dispatch) => {
        axios.post(ADD_PAYMENT_API, body, config)
            .then(response => {
                // console.log(response);
                dispatch({
                    type: ADD_PAYMENT,
                    payload: response.data,
                });
                return response;
            })
            .catch(error => {
                console.log(error);
                dispatch({
                    type: ADD_PAYMENT_ERROR,
                    payload: error,
                });
            });
    }
}