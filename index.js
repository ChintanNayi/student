/**
 * @format
 */
 import React from 'react';
import {AppRegistry} from 'react-native';
import App from './App';
import 'react-native-gesture-handler';
import {name as appName} from './app.json';
import {Provider} from 'react-redux';
import configureStore from "./src/Redux/Store/index"

const store = configureStore()

const StudentRedux = () =>
    <Provider store={store}>
        <App />
    </Provider>

AppRegistry.registerComponent(appName, () => StudentRedux);
