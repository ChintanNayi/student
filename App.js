/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from "react";
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from "react-native";

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from "react-native/Libraries/NewAppScreen";
import EnterOTP from "./src/Screens/LoginScreens/EnterOTP";
import GetOneTimeOTP from "./src/Screens/LoginScreens/GetOneTimeOTP";
import Login from "./src/Screens/LoginScreens/Login";
import WelcomeScreen from "./src/Screens/LoginScreens/WelcomeScreen";
import ForgetPassword from "./src/Screens/LoginScreens/ForgetPassword";
import EnterNewPassword from "./src/Screens/LoginScreens/EnterNewPassword";
import ForgotPasswordOTP from "./src/Screens/LoginScreens/ForgotPasswordOTP";

import Dashboard from "./src/Screens/Components/Dashboard";
import TimeSheet from "./src/Screens/Components/TimeSheets";
import PaymentList from "./src/Screens/Components/PaymentsList";
import FeedBack from "./src/Screens/Components/FeedBack";
import History from "./src/Screens/Components/History";
import MakePayment from "./src/Screens/Components/MakePayment";
import PayNow from "./src/Screens/Components/PayNow";
import Profile from "./src/Screens/Components/Profile";
import CommonHeader from "./src/Screens/CommonComponents/Header";

import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NativeBaseProvider } from "native-base";
import { useSelector } from "react-redux";

const Stack = createNativeStackNavigator();

const HomeScreen = () => {
  return (
    <NativeBaseProvider>
      <Stack.Navigator>
        <Stack.Screen
          options={{ headerShown: false }}
          name="Dashboard"
          component={Dashboard}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="Timesheet"
          component={TimeSheet}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="PaymentList"
          component={PaymentList}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="FeedBack"
          component={FeedBack}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="History"
          component={History}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="MakePayment"
          component={MakePayment}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="PayNow"
          component={PayNow}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="Profile"
          component={Profile}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="Header"
          component={CommonHeader}
        />
      </Stack.Navigator>
    </NativeBaseProvider>
  );
};

const LoginScreen = () => {
  return (
    <NativeBaseProvider>
      <Stack.Navigator>
        <Stack.Screen
          options={{ headerShown: false }}
          name="Login"
          component={Login}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="ForgetPassword"
          component={ForgetPassword}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="EnterOTP"
          component={EnterOTP}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="SendOTP"
          component={GetOneTimeOTP}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="ForgotPasswordOTP"
          component={ForgotPasswordOTP}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="NewPassword"
          component={EnterNewPassword}
        />
      </Stack.Navigator>
    </NativeBaseProvider>
  );
};

const App = () => {
  const auth = useSelector((state) => state.auth);
  const { isAuthenticated, isVerified, token, user } = auth;
  // console.log("APP JS USER DATA", user)
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={user ? "HomeScreen" : "LoginFlow"}>
        <Stack.Screen
          name="Welcome"
          component={WelcomeScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="LoginFlow"
          component={LoginScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="HomeScreen"
          component={HomeScreen}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: "600",
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: "400",
  },
  highlight: {
    fontWeight: "700",
  },
});

export default App;
